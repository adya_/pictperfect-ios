//
//  PPRecoveryViewController.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPRecoveryViewController.h"
#import "PPLoginManager.h"
#import "TSNotifier.h"
#import "TSUtils.h"


@interface PPRecoveryViewController (LoginManagerDelegate) <PPLoginCallback>
@end

@implementation PPRecoveryViewController

- (void)viewDidLoad{
    [super viewDidLoad];
}

-(void) viewDidAppear:(BOOL)animated{
    [PPLoginManager sharedManager].delegate = self;
}

- (IBAction)resetClick:(id)sender {
    
    NSString* email = trim(self.tfEmail.text);
    self.tfEmail.text = email;
    
    PPLoginManager* manager = [PPLoginManager sharedManager];
    if ([manager isValidEmail:email]){
        [self dismissKeyboard];
        [TSNotifier showProgressOnView:self.view];
        [manager performPasswordRecoveryForUser:email];
    }
}

-(void) onPasswordResetResult:(BOOL)success orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [TSNotifier notify:@"Your password has been reset. Check your email for details."];
    }
    else{
        [TSNotifier notifyError:error];
    }
}

@end
