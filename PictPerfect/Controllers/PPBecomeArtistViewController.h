//
//  PPBecomeArtistViewController.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPBecomeArtistViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *tfEmail;
@property (strong, nonatomic) IBOutlet UITextField *tfPortfolio;
@property (strong, nonatomic) IBOutlet UITextView *tfDescr;

@end
