//
//  PPLoginViewController.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSViewController.h"

@interface PPLoginViewController : TSViewController
@property (strong, nonatomic) IBOutlet UITextField *tfEmail;
@property (strong, nonatomic) IBOutlet UITextField *tfPassword;

@property (strong, nonatomic) IBOutlet UIView *lRecovery;

@property (strong, nonatomic) IBOutlet UIButton *bRegister;
@property (strong, nonatomic) IBOutlet UIButton *bLogin;

@end
