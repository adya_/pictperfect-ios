//
//  PPRegistrationViewController.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPRegistrationViewController.h"
#import "PPLoginManager.h"
#import "TSUtils.h"
#import "TSNotifier.h"
#import "PPStorage.h"

@interface PPRegistrationViewController (LoginManagerDelegate) <PPLoginCallback>
@end

@implementation PPRegistrationViewController

-(void) viewDidAppear:(BOOL)animated{
    [PPLoginManager sharedManager].delegate = self;
}

-(void) onLoginResult:(BOOL)success withUser:(PPUser *)user orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [self performSegueWithIdentifier:@"segRegistrationUser" sender:self];
    }
    else{
        [TSNotifier notifyError:error];
    }
    
}
- (IBAction)signupClick:(id)sender {
    NSString* first = trim(self.tfFirstName.text);
    NSString* last = trim(self.tfLastName.text);
    NSString* email = trim(self.tfEmail.text);
    NSString* password = self.tfPassword.text;
    NSString* passwordConfirm = self.tfPasswordConfirm.text;
    
    self.tfFirstName.text = first;
    self.tfLastName.text = last;
    self.tfEmail.text =email;
    PPLoginManager* manager = [PPLoginManager sharedManager];
    
    if (first.length > 1 &&
        last.length > 1 &&
        [manager isValidEmail:email] &&
        [manager isValidPassword:password] &&
        [password isEqualToString:passwordConfirm]){
        [TSNotifier showProgressOnView:self.view];
        [manager performRegistrationWithUsername:email andPassword:password firstName:first andLastName:last];
    }
        
}

                                            
@end
