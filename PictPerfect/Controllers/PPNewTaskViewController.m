//
//  PPNewTaskViewController.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPNewTaskViewController.h"
#import "PPRequestManager.h"
#import "PPLoginManager.h"
#import "TSUtils.h"
#import "TSNotifier.h"
#import "PPUser.h"
#import "PPPicture.h"

@interface PPNewTaskViewController (ChosingImageDelegate) <UIActionSheetDelegate, UIImagePickerControllerDelegate>

@end

@implementation PPNewTaskViewController{
    NSURL* imagePath;
    UIImage* image;
    int rotationAngle;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}
- (IBAction)chooseImageDialog:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select source" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    [actionSheet showInView:self.view];
    }
    else{
        [self showLibrary];
    }
}

-(void) showLibrary{
    UIImagePickerController* pickerView = [UIImagePickerController new];
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:pickerView animated:YES completion:nil];
}

-(void) showCamera{
    UIImagePickerController* pickerView = [UIImagePickerController new];
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypeCamera];
    [self presentViewController:pickerView animated:YES completion:nil];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:{ // Open gallery
            [self showLibrary];
            break;
        }
        case 1:{
            // Open Camera
            [self showCamera];
            break;
        }
        default:
            break;
    }
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    image = [info valueForKey:UIImagePickerControllerOriginalImage];
    imagePath = [info valueForKey:UIImagePickerControllerReferenceURL];
    self.ivPreview.image = image;
    self.lResolution.text = [NSString stringWithFormat:@"%.0fx%.0f", image.size.width, image.size.height];
    NSData* data = UIImageJPEGRepresentation(image, 0.7);
    CGFloat size = [data length];
    NSArray* suffix = @[@"B", @"Kb", @"Mb", @"Gb"];
    int power = 0;
    while (size >= 1024.0) {
        size /=1024.0;
        ++power;
    }
    self.lSize.text = [NSString stringWithFormat:@"%.2f %@", size, suffix[power]];
}

- (IBAction)uploadClicked:(id)sender {
    NSString* title = trim(self.tfTitle.text);
    NSString* details = trim(self.tfDescr.text);
    int deadline = [self.tfDeadline.text intValue];
    
    self.tfTitle.text = title;
    self.tfDescr.text = details;
    
    if (title.length > 1 &&
        imagePath != nil &&
        image != nil &&
        deadline > 0 && deadline < 366){ // max one year.
        [TSNotifier showProgressOnView:self.view withMessage:@"Uploading image.."];
        [[PPRequestManager sharedManager] requestAddImage:image named:APP_NAME andRotationAngle:rotationAngle withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
            [TSNotifier hideProgressOnView:self.view];
            if (success){
                [TSNotifier showProgressOnView:self.view withMessage:@"Uploading Pict.."];
                PPPicture* pict = (PPPicture*)object;
                [[PPRequestManager sharedManager] requestAddTaskWithTitle:title description:details imageId:pict.ID deadline:deadline forUser:[[PPLoginManager sharedManager] getUser].token withResponseCallback:^(BOOL success, TSError *error) {
                    [TSNotifier hideProgressOnView:self.view];
                    if (success) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    else
                        [TSNotifier notifyError:error];
                }];
            }
            else{
                [TSNotifier notifyError:error];
            }
        }];
    }
    else{
        [TSNotifier notify:@"Required field was not set."];
    }
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (range.length + range.location > textField.text.length) return NO;
    
    if (![textField isEqual:self.tfDeadline])
        return YES;
    
    NSUInteger newLength = textField.text.length + string.length - range.length;
    return newLength <= 3;
}


@end
