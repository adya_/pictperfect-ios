
#import "PPArtistTaskViewController.h"
#import "PPLoginManager.h"
#import "PPRequestManager.h"
#import "PPUser.h"
#import "PPStorage.h"
#import "PPTask.h"
#import "PPPicture.h"
#import "PPTaskManager.h"
#import "PPCommentCell.h"
#import "PPComment.h"
#import "PPBid.h"
#import "TSNotifier.h"
#import "TSCurrencyTextField.h"
#import "MGSwipeButton.h"
#import "MFImagePopupViewController.h"

#define MIN_BID_PRICE 0.99f
#define RECEIVE_BID_PRICE(price) price * 0.5f

#define HEADER_VIEW_SECTION_HEIGHT 162.0f
#define MARGIN 10.0f

@interface PPArtistTaskViewController (PPTaskManagerDelegate)<PPTaskManagerCallbacks>

@end

@implementation PPArtistTaskViewController{
    
    IBOutlet UIView *header;
    
    IBOutlet UIView *headerImage;
    IBOutlet UIImageView *ivPicture;
    IBOutlet UILabel *lStatus;
    IBOutlet UILabel *lTitle;
    IBOutlet UILabel *lDescr;
    IBOutlet UIScrollView *headerScroll;
    IBOutlet UILabel *lDeadline;
    
    IBOutlet UIView *commentView;
    IBOutlet UIButton *bChooseImage;
    IBOutlet UIImageView *ivChoosedImage;
    IBOutlet UIButton *bRemoveImage;
    IBOutlet UIImageView *ivRemoveImage;
    IBOutlet UITextView *tvCommentText;
    IBOutlet UIButton *bSend;
    
    IBOutlet UITableView *tableView;
    
    
    IBOutlet UIView *headerBid;
    IBOutlet TSCurrencyTextField *tvBid;
    IBOutlet UIButton *bPlaceBid;
    IBOutlet UIButton *bCancelBid;
    IBOutlet UILabel *lReceive;
    
    UIImage* commentImage;
    PPTaskManager* taskManager;
    UIRefreshControl* refreshControl;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    int taskId = [[[PPStorage sharedStorage] getValueForKey:STORAGE_VALUE_SELECTED_TASK_ID] intValue];
    taskManager = [[PPTaskManager alloc] initWithUser:[[PPLoginManager sharedManager] getUser]];
    taskManager.delegate = self;
    [TSNotifier showProgressOnView:self.view withMessage:@"Loading Pict Details.."];
    [taskManager getTaskDetailsForTaskWithId:taskId];
    tvBid.maximumLength = 10;
    header.hidden = YES;
    tableView.tableHeaderView = header;
    refreshControl = [UIRefreshControl new];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Updating Pict.."];
    [refreshControl addTarget:self action:@selector(refreshTask) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControl];
}

-(void) refreshTask{
    [taskManager getTaskDetailsForTaskWithId:taskManager.task.ID];
}

-(UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return taskManager.commentsCount;
}
                   
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* commentCellIdentifier = @"PPCommentCell";
    PPCommentCell* cell = (PPCommentCell*)[tableView dequeueReusableCellWithIdentifier:commentCellIdentifier];
    if (cell == nil){
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"PPCommentCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (taskManager.isTaken && !taskManager.isFinished && indexPath.row == 0 && ![taskManager commentAtIndex:indexPath.row].isArtist){
        [cell setRightButtons:@[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:STORAGE_COLORS_DELETE_COLOR callback:^BOOL(MGSwipeTableCell *sender) {
            
            [TSNotifier showProgressOnView:self.view withMessage:@"Removing Comment"];
            [taskManager removeCommentAtIndex:indexPath.row];
            return YES;
        }]]];
    }
    [cell showOwnership:taskManager.isTaken withRole:YES];
    [cell setComment:[taskManager commentAtIndex:indexPath.row]];
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PPCommentCell calculateHeightWithComment:[taskManager commentAtIndex:indexPath.row]];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PPCommentCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    [self enlargeCommentImage:[taskManager commentAtIndex:indexPath.row].picture];
}
- (IBAction)enlargeImage:(id)sender {    MFImagePopupViewController* controller = [[MFImagePopupViewController alloc] initWithNibName:@"MFImagePopupViewController" bundle:nil];
    controller.delegate =self;
    PPPicture* pict = taskManager.task.picture;
    [controller setImageWithURL:pict.imageURL canSave:taskManager.isTaken];
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationFade];
}


-(void) enlargeCommentImage:(PPPicture*)picture{
    if (!picture.isValid) return;
    MFImagePopupViewController* controller = [[MFImagePopupViewController alloc] initWithNibName:@"MFImagePopupViewController" bundle:nil];
    
    [controller setImageWithURL:picture.imageURL canSave:taskManager.isTaken];
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationFade];
}



-(void) updateTaskView{
    [[PPRequestManager sharedManager] requestImageFromURL:taskManager.task.picture.imageURL forImageView:ivPicture withPlaceholder:nil];
    if (taskManager.isTaken)
        lStatus.text = [NSString stringWithFormat:@"Status: %@",taskManager.task.statusString];
    else
        lStatus.text = nil;
    
    lTitle.text = taskManager.task.title;
    lDescr.text = taskManager.task.description;
    lDeadline.hidden = !taskManager.isTaken;
    if (taskManager.isTaken){
        int hours = taskManager.task.timeLeft;

        if (hours < 0){
            lDeadline.text = @"Deadline Reached!";
            lDeadline.textColor = STORAGE_COLORS_DEADLINE_REACHED;
        }else if (hours <= 24){
            lDeadline.text = @"Deadline: 1 day!";
            lDeadline.textColor = STORAGE_COLORS_DEADLINE_RUNS_OUT;
        }
        else{
            int days = ceil(hours / 24.0f);
            lDeadline.text = [NSString stringWithFormat:@"Deadline: %d days", days];
            lDeadline.textColor = STORAGE_COLORS_DEADLINE_DEFAULT;
        }
    }

}

-(void) updateBidsView{
    
    headerBid.hidden = taskManager.isTaken;
    
    bCancelBid.hidden = (taskManager.placedBid == nil);
    if (taskManager.placedBid != nil){
       
        [tvBid setAmount:@(taskManager.placedBid.price)];
        [self setReceiveLabel:taskManager.placedBid.price];
        [bPlaceBid setTitle:@"Change Bid"  forState:UIControlStateNormal];
    }
    else{
        tvBid.amount = 0;
         [self setReceiveLabel:0];
        [bPlaceBid setTitle:@"Place Bid"  forState:UIControlStateNormal];
    }
}

-(void) layoutHeader{
    [lTitle sizeToFit];
    CGRect frame = lDescr.frame;
    frame.origin.y = lTitle.frame.origin.y + lTitle.frame.size.height + MARGIN;
    lDescr.frame = frame;
    [lDescr sizeToFit];
    
    CGFloat deadlineHeight = (lDeadline.isHidden?0:lDeadline.frame.size.height);
    
    // adjust scroll position.
    frame = headerScroll.frame;
    frame.origin.y = lDeadline.frame.origin.y +deadlineHeight;
    headerScroll.frame = frame;
    
    CGFloat dstHeight = (MARGIN + lTitle.frame.size.height + MARGIN + lDescr.frame.size.height + MARGIN);
    
    if (dstHeight > headerScroll.frame.size.height){
        [headerScroll setContentSize:CGSizeMake(headerScroll.frame.size.width, dstHeight)];
        frame = headerImage.frame;
        frame.size.height = 2 * HEADER_VIEW_SECTION_HEIGHT + deadlineHeight;
        headerImage.frame = frame;
    }
    else{
        frame = headerScroll.frame;
        frame.size.height = dstHeight;
        headerScroll.frame  = frame;
        
        frame = headerImage.frame;
        frame.size.height = HEADER_VIEW_SECTION_HEIGHT + headerScroll.frame.size.height +deadlineHeight;
        headerImage.frame = frame;
    }
    
    // place headerBid right after headerImage
    frame = headerBid.frame;
    frame.origin.y = headerImage.frame.size.height;
    headerBid.frame = frame;
    CGFloat bidHeight = 0;
    if (taskManager.isTaken) { // hide if taken
        bidHeight = 0;
    } else{
        if (taskManager.placedBid){
            bidHeight = HEADER_VIEW_SECTION_HEIGHT;
        }
        else{
            bidHeight = HEADER_VIEW_SECTION_HEIGHT - MARGIN - bCancelBid.frame.size.height;
        }
    }
    
    // calculate final header height
    frame = header.frame;
    frame.size.height = headerImage.frame.size.height + bidHeight;
    header.frame = frame;
    
    // Hide/Show commentView and Expand/Collapse tableView
    commentView.hidden = taskManager.isFinished;
    if (taskManager.isFinished){
        frame = tableView.frame;
        frame.size.height +=commentView.frame.size.height;
        tableView.frame = frame;
    }
}

-(void) setReceiveLabel:(double) receive{
    lReceive.text = [NSString stringWithFormat:@"(You Will Receive $%.2f)", RECEIVE_BID_PRICE(receive)];
}

- (IBAction)placeBidClicked:(id)sender {
    double price = [tvBid.amount doubleValue];
    if (price <= MIN_BID_PRICE || RECEIVE_BID_PRICE(price) <= 0){
        [TSNotifier notify:[NSString stringWithFormat:@"Bid must be greater than $%.2f", MIN_BID_PRICE]];
        return;
    }
        
    if (taskManager.placedBid == nil || [tvBid.amount doubleValue] != taskManager.placedBid.price){
        [TSNotifier showProgressOnView:self.view withMessage:@"Processing Bid.."];
            [taskManager placeBidWithPrice:[tvBid.amount doubleValue]];
    }
}
- (IBAction)cancelBidClicked:(id)sender {
    if (taskManager.placedBid != nil){
        [TSNotifier showProgressOnView:self.view withMessage:@"Canceling Bid.."];
        [taskManager cancelBid];
    }
}

- (IBAction)priceEntered:(id)sender {
    [self setReceiveLabel:[tvBid.amount doubleValue]];
}

- (IBAction)chooseImageClicked:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select source" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
        [actionSheet showInView:self.view];
    }
    else{
        [self showLibrary];
    }
}

-(void) showLibrary{
    UIImagePickerController* pickerView = [UIImagePickerController new];
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:pickerView animated:YES completion:nil];
}

-(void) showCamera{
    UIImagePickerController* pickerView = [UIImagePickerController new];
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypeCamera];
    [self presentViewController:pickerView animated:YES completion:nil];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:{ // Open gallery
            [self showLibrary];
            break;
        }
        case 1:{
            // Open Camera
            [self showCamera];
            break;
        }
        default:
            break;
    }
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    commentImage = [info valueForKey:UIImagePickerControllerOriginalImage];
   
    ivChoosedImage.image = commentImage;
    bRemoveImage.hidden = NO;
    ivRemoveImage.hidden = NO;
}

- (IBAction)removeImageClicked:(id)sender {
    [self resetAttachment];
}
- (IBAction)sendClicked:(id)sender {
    NSString* comment = trim(tvCommentText.text);
    if (comment.length > 0){
        if (commentImage == nil)
            [TSNotifier showProgressOnView:self.view withMessage:@"Sending Comment.."];
        else
            [TSNotifier showProgressOnView:self.view withMessage:@"Uploading Picture.."];
        [taskManager addCommentWithMeassage:comment andPicture:commentImage];
    }
    else{
        [TSNotifier notify:@"Comment can't be empty"];
    }
}

-(void) resetAttachment{
    commentImage = nil;
    ivChoosedImage.image = [UIImage imageNamed:@"plus2"];
    bRemoveImage.hidden =YES;
    ivRemoveImage.hidden = YES;
}

-(void) onTaskDetailsReceived:(PPTask *)task{
    [TSNotifier hideProgressOnView:self.view];
    [refreshControl endRefreshing];
       [tableView reloadData];
    [self updateTaskView];
    [self updateBidsView];
    [self layoutHeader];
    header.hidden = NO;
 
}

-(void) onBidCanceled{
    [TSNotifier hideProgressOnView:self.view];
    [self updateBidsView];
    [self layoutHeader];
}

-(void) onBidPlaced:(PPBid *)placedBid{
    [TSNotifier hideProgressOnView:self.view];
    [self updateBidsView];
    [self layoutHeader];
}

-(void) onCommentPictureUploaded:(PPPicture *)picture{
    [TSNotifier hideProgressOnView:self.view];
    [TSNotifier showProgressOnView:self.view withMessage:@"Sending Comment.."];
}

-(void) onCommentAdded{
    [TSNotifier hideProgressOnView:self.view];
    [self resetAttachment];
    tvCommentText.text = nil;
    [tableView reloadData];
    [self updateTaskView];
    [self layoutHeader];
}

-(void) onCommentRemoved{
    [TSNotifier hideProgressOnView:self.view];
    [tableView reloadData];
    [self updateTaskView];
    [self layoutHeader];
}

-(void) onOperationFailed:(TaskOperation)operation withError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    [refreshControl endRefreshing];
    [TSNotifier notifyError:error];
}

@end
