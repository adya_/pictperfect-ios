#import "PPUserTaskDetailsViewController.h"

#import "PPRequestManager.h"
#import "PPArtist.h"
#import "PPStorage.h"
#import "PPTask.h"
#import "PPBid.h"
#import "PPPicture.h"
#import "PPTaskManager.h"
#import "PPCommentCell.h"
#import "PPComment.h"
#import "TSNotifier.h"
#import "MBProgressHUD.h"
#import "DYRateView.h"
#import "MGSwipeButton.h"
#import "MFImagePopupViewController.h"

#define MIN_BID_PRICE 0.99f
#define RECEIVE_BID_PRICE(price) price * 0.5f

#define HEADER_VIEW_SECTION_HEIGHT 162.0f
#define MARGIN 10.0f

@interface PPUserTaskDetailsViewController (PPTaskManagerDelegate)<PPTaskManagerCallbacks>


@end

@implementation PPUserTaskDetailsViewController{
    
    IBOutlet UIView *header;
    IBOutlet UIScrollView *headerScroll;
    
    IBOutlet UIView *rateView;
    IBOutlet UIView *vRatePlaceholder;
    IBOutlet UIButton *bAcceptRate;
    DYRateView* dyRateView;
    MBProgressHUD *hud;
    
    IBOutlet UIImageView *ivPicture;
    IBOutlet UILabel *lStatus;
    IBOutlet UILabel *lResponsibleArtist;
    IBOutlet UILabel *lTitle;
    IBOutlet UILabel *lDescr;
    IBOutlet UIButton *bAcceptPict;
    
    IBOutlet UILabel *lDeadline;
    IBOutlet UITableView *tableView;
    
    IBOutlet UIView *commentView;
    IBOutlet UIButton *bSend;
    IBOutlet UITextView *tvCommentText;
    IBOutlet UIImageView *ivChoosenImage;
    IBOutlet UIButton *bChoseImage;
    IBOutlet UIImageView *ivRemoveImage;
    IBOutlet UIButton *bRemoveImage;
    
    UIImage* commentImage;
    PPTaskManager* taskManager;
    UIRefreshControl* refreshControl;
}

@synthesize  taskManager;

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    header.hidden = YES;
    tableView.tableHeaderView = header;
    refreshControl = [UIRefreshControl new];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Updating Pict.."];
    [refreshControl addTarget:self action:@selector(refreshTask) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControl];
    
    dyRateView = [[DYRateView alloc] initWithFrame:vRatePlaceholder.frame];
    dyRateView.editable = YES;
    dyRateView.rate = 2;
    [rateView addSubview:dyRateView];
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enlargeImage:)];
    [ivPicture addGestureRecognizer:tap];
}

-(void) refreshTask{
    [taskManager getTaskDetailsForTaskWithId:taskManager.task.ID];
}

-(UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return taskManager.commentsCount;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* commentCellIdentifier = @"PPCommentCell";
    PPCommentCell* cell = (PPCommentCell*)[tableView dequeueReusableCellWithIdentifier:commentCellIdentifier];
    if (cell == nil){
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"PPCommentCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (taskManager.isTaken && !taskManager.isFinished && indexPath.row == 0 && ![taskManager commentAtIndex:indexPath.row].isArtist){
        [cell setRightButtons:@[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:STORAGE_COLORS_DELETE_COLOR callback:^BOOL(MGSwipeTableCell *sender) {
            
            [TSNotifier showProgressOnView:self.view withMessage:@"Removing Comment"];
            [taskManager removeCommentAtIndex:indexPath.row];
            return YES;
        }]]];
    }

    [cell showOwnership:taskManager.isTaken withRole:NO]; // we are in user task conrtoller :)
    [cell setComment:[taskManager commentAtIndex:indexPath.row]];
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PPCommentCell calculateHeightWithComment:[taskManager commentAtIndex:indexPath.row]];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PPCommentCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    [self enlargeCommentImage:[taskManager commentAtIndex:indexPath.row].picture];
}
- (IBAction)enlargeImage:(id)sender {
   MFImagePopupViewController* controller = [[MFImagePopupViewController alloc] initWithNibName:@"MFImagePopupViewController" bundle:nil];
    controller.delegate = self;
    PPPicture* pict = taskManager.task.picture;
    [controller setImageWithURL:pict.imageURL canSave:YES];
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationFade];
}


-(void) updateTaskView{
    [[PPRequestManager sharedManager] requestImageFromURL:taskManager.task.picture.imageURL forImageView:ivPicture withPlaceholder:nil];
    if (taskManager.isPendingArtist)
        lStatus.text = [NSString stringWithFormat:@"Status: %@",@"Artist Bid Received"];
    else
        lStatus.text = [NSString stringWithFormat:@"Status: %@",taskManager.task.statusString];
    
    if (taskManager.confirmedBid != nil){
        lResponsibleArtist.text = [NSString stringWithFormat:@"Artist: %@ %@", taskManager.confirmedBid.artist.firstName, taskManager.confirmedBid.artist.lastName];
    }
    else{
        lResponsibleArtist.text = nil;
    }
    lTitle.text = taskManager.task.title;
    lDescr.text = taskManager.task.description;
    lDeadline.hidden = !taskManager.isTaken;
    if (taskManager.isTaken){
        int hours = taskManager.task.timeLeft;
        
        if (hours < 0){
            lDeadline.text = @"Deadline Reached!";
            lDeadline.textColor = STORAGE_COLORS_DEADLINE_REACHED;
        }else if (hours <= 24){
            lDeadline.text = @"Deadline: 1 day!";
            lDeadline.textColor = STORAGE_COLORS_DEADLINE_RUNS_OUT;
        }
        else{
            int days = ceil(hours / 24.0f);
            lDeadline.text = [NSString stringWithFormat:@"Deadline: %d days", days];
            lDeadline.textColor = STORAGE_COLORS_DEADLINE_DEFAULT;
        }
    }
    commentView.hidden = taskManager.isFinished;
    
    bAcceptPict.hidden = !(taskManager.isTaken && taskManager.isProgressed && !taskManager.isFinished && taskManager.hasArtistPicture);
}


-(void) enlargeCommentImage:(PPPicture*)picture{
    if (!picture.isValid) return;
    MFImagePopupViewController* controller = [[MFImagePopupViewController alloc] initWithNibName:@"MFImagePopupViewController" bundle:nil];

    [controller setImageWithURL:picture.imageURL canSave:taskManager.isTaken];
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationFade];
}


-(void) layoutHeader{
    [lTitle sizeToFit];
    CGRect frame = lDescr.frame;
    frame.origin.y = lTitle.frame.origin.y + lTitle.frame.size.height + MARGIN;
    lDescr.frame = frame;
    [lDescr sizeToFit];
    CGFloat dstHeight = (MARGIN + lTitle.frame.size.height + MARGIN + lDescr.frame.size.height + MARGIN);
    CGFloat deadlineHeight = (lDeadline.isHidden?0:lDeadline.frame.size.height);
    // adjust scroll position.
    frame = headerScroll.frame;
    frame.origin.y = lDeadline.frame.origin.y +deadlineHeight;
    headerScroll.frame = frame;
    
    if (dstHeight > headerScroll.frame.size.height){
        [headerScroll setContentSize:CGSizeMake(headerScroll.frame.size.width, dstHeight)];
        frame = header.frame;
        frame.size.height = 2 * HEADER_VIEW_SECTION_HEIGHT + deadlineHeight;
        header.frame = frame;
    }
    else{
        frame = headerScroll.frame;
        frame.size.height = dstHeight;
        headerScroll.frame  = frame;
        
        frame = header.frame;
        frame.size.height = HEADER_VIEW_SECTION_HEIGHT + headerScroll.frame.size.height + lDeadline.frame.size.height;
        header.frame = frame;
    }
    
    //  Expand tableView if commentView is hidden.
    if (commentView.hidden){
        frame = tableView.frame;
        frame.size.height +=commentView.frame.size.height;
        tableView.frame = frame;
    }
}

- (IBAction)chooseImageClicked:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select source" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
        [actionSheet showInView:self.view];
    }
    else{
        [self showLibrary];
    }
}

-(void) showLibrary{
    UIImagePickerController* pickerView = [UIImagePickerController new];
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:pickerView animated:YES completion:nil];
}

-(void) showCamera{
    UIImagePickerController* pickerView = [UIImagePickerController new];
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypeCamera];
    [self presentViewController:pickerView animated:YES completion:nil];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:{ // Open gallery
            [self showLibrary];
            break;
        }
        case 1:{
            // Open Camera
            [self showCamera];
            break;
        }
        default:
            break;
    }
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    commentImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    ivChoosenImage.image = commentImage;
    bRemoveImage.hidden = NO;
    ivRemoveImage.hidden = NO;
}

- (IBAction)removeImageClicked:(id)sender {
    [self resetAttachment];
}

- (IBAction)sendClicked:(id)sender {
    NSString* comment = trim(tvCommentText.text);
    if (comment.length > 0){
        if (commentImage == nil)
            [TSNotifier showProgressOnView:self.view withMessage:@"Sending Comment.."];
        else
            [TSNotifier showProgressOnView:self.view withMessage:@"Uploading Picture.."];
        [taskManager addCommentWithMeassage:comment andPicture:commentImage];
    }
    else{
        [TSNotifier notify:@"Comment can't be empty"];
    }
}

-(void) resetAttachment{
    commentImage = nil;
    ivChoosenImage.image = [UIImage imageNamed:@"plus2"];
    bRemoveImage.hidden =YES;
    ivRemoveImage.hidden = YES;
}

- (IBAction)acceptClicked:(id)sender {
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = rateView;
}
- (IBAction)acceptRateClick:(id)sender {
    [hud hide:YES];
    [TSNotifier showProgressOnView:self.view withMessage:@"Accepting Pict.."];
        [taskManager acceptTaskWithArtistRating:(int)dyRateView.rate];
}


-(void) onTaskDetailsReceived:(PPTask *)task{
    [TSNotifier hideProgressOnView:self.view];
    [self updateTaskView];
    [self layoutHeader];
    [refreshControl endRefreshing];
    header.hidden = NO;
    [tableView reloadData];
}

-(void) onBidSelected:(PPBid *)selectedBid{
    [self updateTaskView];
}

-(void) onTaskCanceled{
    [self updateTaskView];
}

-(void) onCommentPictureUploaded:(PPPicture *)picture{
    [TSNotifier hideProgressOnView:self.view];
    [TSNotifier showProgressOnView:self.view withMessage:@"Sending Comment.."];
}

-(void) onCommentAdded{
    [TSNotifier hideProgressOnView:self.view];
    [self resetAttachment];
    tvCommentText.text = nil;
    [tableView reloadData];
    [self updateTaskView];
    [self layoutHeader];
}

-(void) onCommentRemoved{
    [TSNotifier hideProgressOnView:self.view];
    [tableView reloadData];
    [self updateTaskView];
    [self layoutHeader];
}

-(void) onOperationFailed:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    [refreshControl endRefreshing];
    [TSNotifier notifyError:error];
}

@end
