//
//  PPNewTaskViewController.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSViewController.h"

@interface PPNewTaskViewController : TSViewController
@property (strong, nonatomic) IBOutlet UITextField *tfTitle;
@property (strong, nonatomic) IBOutlet UITextView *tfDescr;
@property (strong, nonatomic) IBOutlet UITextField *tfDeadline;

@property (strong, nonatomic) IBOutlet UIImageView *ivPreview;

@property (strong, nonatomic) IBOutlet UILabel *lName;
@property (strong, nonatomic) IBOutlet UILabel *lResolution;
@property (strong, nonatomic) IBOutlet UILabel *lSize;

@end
