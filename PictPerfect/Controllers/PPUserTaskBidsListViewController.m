
#import "PPUserTaskBidsListViewController.h"
#import "PPBidCell.h"
#import "PPBid.h"
#import "PPTaskManager.h"
#import "PPTask.h"
#import "MGSwipeButton.h"
#import "PPStorage.h"
#import "PPPaymentManager.h"
#import "TSNotifier.h"

@interface PPUserTaskBidsListViewController (PPTaskManagerDelegate) <PPTaskManagerCallbacks, PPPaymentManagerDelegate>

@end

@implementation PPUserTaskBidsListViewController{
    
    IBOutlet UILabel *lNoBids;
    IBOutlet UITableView *bidsTableView;
    UIRefreshControl* refreshControl;
    
    PPPaymentResult* storedResult;
}

@synthesize taskManager;

- (void)viewDidLoad{
    [super viewDidLoad];
    refreshControl = [UIRefreshControl new];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Updating Pict.."];
    [refreshControl addTarget:self action:@selector(refreshTask) forControlEvents:UIControlEventValueChanged];
    [bidsTableView addSubview:refreshControl];
}

-(void) refreshTask{
    [taskManager getTaskDetailsForTaskWithId:taskManager.task.ID];
}



-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1.0f;
}

-(UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (taskManager.confirmedBid != nil)
        return 1;
    else
        return taskManager.bidsCount;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PPBidCell height];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* bidCellIdentifier = @"PPBidCell";
    PPBidCell* cell = (PPBidCell*)[bidsTableView dequeueReusableCellWithIdentifier:bidCellIdentifier];
    if (cell == nil){
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"PPBidCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    PPBid* bid = taskManager.confirmedBid;
    if (bid == nil){
        bid = [taskManager bidAtIndex:indexPath.row];
    }
    if (taskManager.confirmedBid == nil && !taskManager.isFinished && taskManager.isPendingArtist){
        PPPaymentResult* payment = [[PPPaymentManager sharedManager] savedPaymentForTask:taskManager.task.ID];
        if (payment == nil){ // ensure there is no payments
            [cell setRightButtons:@[[MGSwipeButton buttonWithTitle:@"Pay" backgroundColor:STORAGE_COLORS_PAY_COLOR callback:^BOOL(MGSwipeTableCell *sender) {
                [self payWithBid:bid];
                return YES;
            }]]];
        }
        else if (payment.bidId == bid.ID){
            [cell setRightButtons:@[[MGSwipeButton buttonWithTitle:@"Verify" backgroundColor:STORAGE_COLORS_VERIFY_COLOR callback:^BOOL(MGSwipeTableCell *sender) {
                [self repeatPayment:payment];
                return YES;
            }]]];
        }
    }
    else if (taskManager.confirmedBid != nil && !taskManager.isFinished && taskManager.isDeadlineReached && !taskManager.hasArtistPicture){
        [cell setRightButtons:@[[MGSwipeButton  buttonWithTitle:@"Revoke" backgroundColor: STORAGE_COLORS_REVOKE_COLOR callback:^BOOL(MGSwipeTableCell *sender) {
            [self revokeTask];
            return YES;
        }]]];
    }
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    [cell setBid:bid withPicture:taskManager.task.picture];
    
    return cell;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PPBidCell* cell = [bidsTableView cellForRowAtIndexPath:indexPath];
    [cell showSwipe:MGSwipeDirectionRightToLeft animated:YES];
}



-(void) payWithBid:(PPBid*)bid{
    PPPaymentManager* manager = [PPPaymentManager sharedManager];
    manager.parentViewController = self.parentViewController;
    manager.delegate = self;
    if (![manager pay:bid.price withPurpose:taskManager.task.title forBid:bid.ID inTask:taskManager.task.ID]){
        [TSNotifier notify:@"Invalid payment operation."];
    }
}

-(void) repeatPayment:(PPPaymentResult*)payment{
    [TSNotifier showProgressOnView:self.view withMessage:@"Verifying Payment.."];
    [taskManager selectBidWithId:payment.bidId andPayAuthId:payment.authorizationId];
}

-(void) revokeTask{
    [TSNotifier showProgressOnView:self.view withMessage:@"Revoking Bid.."];
    [taskManager cancelTask];
}

-(void) onTaskDetailsReceived:(PPTask *)task{
    [refreshControl endRefreshing];
    lNoBids.hidden = (taskManager.bidsCount != 0);
    [bidsTableView reloadData];
}

-(void) onTaskCanceled{
    [bidsTableView reloadData];
}

-(void) onTaskAccepted{
    [bidsTableView reloadData];
}

-(void) onBidSelected:(PPBid *)selectedBid{
    [TSNotifier hideProgressOnView:self.view];
    [[PPPaymentManager sharedManager] removePayment:storedResult];
    storedResult = nil;
    [bidsTableView reloadData];
}

-(void) onOperationFailed:(TaskOperation)operation withError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    [TSNotifier notifyError:error];
    if (operation == TASK_OP_BID_SELECT){
        [TSNotifier notify:@"Payment was not ended successfuly. However, it has been saved and will be sent next time. Or you can try Verify it now."];
        [bidsTableView reloadData];
    }
}

-(void) onPaymentCompleted:(PPPaymentResult *)result{
    PPPaymentManager* manager = [PPPaymentManager sharedManager];
    [manager savePayment:result];
    storedResult = result;
    [self repeatPayment:storedResult];
}

-(void) onPaymentCanceled{
    [TSNotifier notify:@"Payment has ben canceled."];
}
@end
