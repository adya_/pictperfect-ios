
#import "PPLoginViewController.h"
#import "PPLoginManager.h"
#import "TSNotifier.h"
#import "TSUtils.h"
#import "PPUser.h"


@interface PPLoginViewController (LoginManagerDelegate)<PPLoginCallback>
@end

@implementation PPLoginViewController

- (void)viewDidLoad{
    [super viewDidLoad];
	PPLoginManager* manager = [PPLoginManager sharedManager];
    if ([manager hasStoredToken]){
        self.view.hidden = YES;
        [TSNotifier showProgressOnView:self.view];
        [manager performLoginWithSavedUser];
    }
}

-(void) viewDidAppear:(BOOL)animated{
    [PPLoginManager sharedManager].delegate = self;
    self.tfEmail.text = [[PPLoginManager sharedManager] getUser].email;
}

- (void) recoveryClick{
    [self performSegueWithIdentifier:@"segLoginRecovery" sender:self];
}

- (IBAction)loginClick:(id)sender {
    NSString* email = trim(self.tfEmail.text);
    self.tfEmail.text = email;
    NSString* password = self.tfPassword.text;
    PPLoginManager* manager = [PPLoginManager sharedManager];
    [self dismissKeyboard];
    if ([manager isValidEmail:email] && [manager isValidPassword:password]) {
        [TSNotifier showProgressOnView:self.view];
        [manager performLoginWithUsername:email andPassword:password];
    }
        
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [self dismissKeyboard];
}
@end

@implementation PPLoginViewController (LoginManagerDelegate)

-(void) onLoginResult:(BOOL)success withUser:(PPUser *)user orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    self.view.hidden = NO;
    if (success){
        if (user.role != BANNED){
            self.tfPassword.text = @""; // clear password
            [self performSegueWithIdentifier:@"segLogin" sender:self];
        }
        else{
            [TSNotifier notifyWithTitle:@"Login failed" message:@"Sorry, you are not alowed to enter PictPerfect."];
        }
    }
    else if (error != nil){
        [TSNotifier notifyError:error];
    }
}



@end
