//
//  PPChangePaswordViewController.m
//  PictPerfect
//
//  Created by Adya on 10/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPChangePasswordViewController.h"
#import "PPLoginManager.h"
#import "TSNotifier.h"

@interface PPChangePasswordViewController (PPLoginCallback) <PPLoginCallback>

@end

@implementation PPChangePasswordViewController

-(void) viewDidAppear:(BOOL)animated{
    [PPLoginManager sharedManager].delegate = self;
}

- (IBAction)doneClicked:(id)sender {
    NSString* old = self.tfOldPassword.text;
    NSString* new = self.tfNewPassword.text;
    NSString* confirm = self.tfConfirmNewPassword.text;
    PPLoginManager* manager = [PPLoginManager sharedManager];
    if ([manager isValidPassword:old] &&
        [manager isValidPassword:new] &&
        [manager isValidPassword:confirm] &&
        [new isEqualToString:confirm]){
        [TSNotifier showProgressOnView:self.view];
        [manager performChangePasswordFromOldPassword:old toNewPassword:new];
    }
    else{
        [TSNotifier notify:@"Some fields has incorrect values."];
    }
}

-(void) onPasswordChangedResult:(BOOL)success orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [TSNotifier notify:@"Your New Password has been Updated!"];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [TSNotifier notifyError:error];
    }
}

@end
