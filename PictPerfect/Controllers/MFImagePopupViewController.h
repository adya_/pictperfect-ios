#import <UIKit/UIKit.h>
#import "UIViewController+MJPopupViewController.h"

@interface MFImagePopupViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *ivImage;
@property (strong, nonatomic) IBOutlet UIButton *bSave;

@property (nonatomic, assign) UIViewController* delegate;

-(void) setImageWithURL:(NSString*) url canSave:(BOOL)canSave;
@end
