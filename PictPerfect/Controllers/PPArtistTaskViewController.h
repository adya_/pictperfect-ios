
#import <UIKit/UIKit.h>
#import "TSViewController.h"

@interface PPArtistTaskViewController : TSViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@end
