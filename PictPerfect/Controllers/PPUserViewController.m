//
//  PPArtistViewController.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPUserViewController.h"
#import "PPMenuManager.h"
#import "PPTaskListManager.h"
#import "PPLoginManager.h"
#import "PPUser.h"
#import "PPTask.h"
#import "TSNotifier.h"
#import "PPTaskCell.h"
#import "PPStorage.h"

@interface PPUserViewController (PPTaskManagerDelgate)<PPTaskManagerDelegate>

@end

@implementation PPUserViewController{
    PPMenuManager* menuManager;
    NSDictionary* taskLists;
    PPTaskListManager* current;
    UIView* progressView; // view on which progress bar is currently showed
    BOOL loadingPage;
    BOOL allPagesLoaded;
    BOOL isScrolling;
    UIRefreshControl* refreshControl;
    NSMutableDictionary* pendingForDeletingTasks;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    PPUser* user = [[PPLoginManager sharedManager] getUser];
    taskLists = @{TASK_LIST_CURRENT : [[PPTaskListManager alloc] initWithUser:user andManagedTaskList:TASK_LIST_CURRENT],
                  TASK_LIST_PENDING : [[PPTaskListManager alloc] initWithUser:user andManagedTaskList:TASK_LIST_PENDING],
                  TASK_LIST_AVAILABLE : [[PPTaskListManager alloc] initWithUser:user andManagedTaskList:TASK_LIST_AVAILABLE],};
    
    self.showBackButton = NO;
    
    pendingForDeletingTasks = [NSMutableDictionary new];
    
    refreshControl = [UIRefreshControl new];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Updating Picts.."];
    [refreshControl addTarget:self action:@selector(refreshTasks) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    [self layoutViewForUser:user];
}

-(void) layoutViewForUser:(PPUser*) user{
    if (user.role == ARTIST){
        [self availableClicked:self.bAvailable];
    }
    else{
        self.tbTabs.hidden = YES;
        [self yourPictsClicked:self.bYourPicts];
        CGRect frame = self.tableView.frame;
        frame.origin.y -= self.tbTabs.frame.size.height;
        frame.size.height += self.tbTabs.frame.size.height;
        self.tableView.frame = frame;
    }
        
}

-(void) refreshTasks{
    loadingPage = YES;
    [current updateTasks];
}

-(void) viewDidAppear:(BOOL)animated{
    menuManager = [[PPMenuManager alloc] initWithViewController:self]; // temp solution to handle when user sent request to becmoe artist
    [self loadTasks];
    
}

-(void) loadTasks{
    loadingPage = YES;
    if ([current getTasks].count != 0){
        [TSNotifier showProgressOnView:self.view withMessage:@"Updating picts.."];
        [current updateTasks];
    }
    else{
        [TSNotifier showProgressOnView:self.view withMessage:@"Loading picts.."];
        [current loadMoreTasks];
    }
}

-(void) updateListNamed:(NSString*)listName withList:(NSArray*)taskList{
    self.lNoTasks.hidden = (taskList.count != 0);
    [self.tableView reloadData];
    if (!self.lNoTasks.hidden){
        if ([TASK_LIST_CURRENT isEqualToString:listName]){
            self.lNoTasks.text = @"You Have Not Created Any Picts Yet.\nTo Do So, Select New Pict.";
        }
        else if ([TASK_LIST_PENDING isEqualToString:listName]){
            self.lNoTasks.text = @"You haven't got any job.\nStart with making a bid to some Pict in Available list.";
        }
        else if ([TASK_LIST_AVAILABLE isEqualToString:listName]){
            self.lNoTasks.text = @"There is no Picts for you right now.\nCheck out later.";
        }
    }

}

-(void) onTaskList:(NSString *)listName loadedList:(NSArray *)taskList hasMore:(BOOL) hasMore{
    [TSNotifier hideProgressOnView:self.view];
    loadingPage = NO;
    allPagesLoaded = !hasMore;
    [self updateListNamed:listName withList:taskList];
    [refreshControl endRefreshing];
}

-(void) onOperationFailedWithError:(TSError *)error{
    if (!progressView)
        progressView = self.view;
    [TSNotifier hideProgressOnView:progressView];
    [TSNotifier notifyError:error];
    progressView = nil;
    loadingPage = NO;
    [refreshControl endRefreshing];
}

-(void) onTaskDeleted:(int) taskId{
    if (!progressView)
        progressView = self.view;
    [TSNotifier hideProgressOnView:progressView];
    progressView = nil;
    NSString* key = [NSString stringWithFormat:@"%d", taskId];
    NSIndexPath* indexPath = [pendingForDeletingTasks valueForKey:key];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [pendingForDeletingTasks removeObjectForKey:key];
}


- (IBAction)menuClick:(id)sender {
    [menuManager toggleMenu];
}

- (IBAction)yourPictsClicked:(id)sender {
    current = [taskLists valueForKey:TASK_LIST_CURRENT];
    current.delegate = self;
    [self loadTasks];
    [self toogleTab:sender];
}

- (IBAction)availableClicked:(id)sender {
    current = [taskLists valueForKey:TASK_LIST_AVAILABLE];
    current.delegate = self;
    [self loadTasks];
    [self toogleTab:sender];
}

- (IBAction)pendingClicked:(id)sender {
    current = [taskLists valueForKey:TASK_LIST_PENDING];
    current.delegate = self;
    [self loadTasks];
    [self toogleTab:sender];
}

-(void) toogleTab:(UIButton*) tabButton{
    [self.bAvailable setSelected:NO];
    [self.bPending setSelected:NO];
    [self.bYourPicts setSelected:NO];
    [tabButton setSelected:YES];
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [current getTasks].count;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PPTaskCell height];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* taskCellIdentifier = @"PPTaskCell";
    PPTaskCell* cell = (PPTaskCell*)[tableView dequeueReusableCellWithIdentifier:taskCellIdentifier];
    if (cell == nil){
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"PPTaskCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    if ([current getTasks].count != 0){
        PPTask* task = [current getTasks][indexPath.row];
       cell.colorTitle = ![current.listName isEqualToString:TASK_LIST_AVAILABLE];
        [cell setTask:task];
    }
    return cell;
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    PPTask* t = [current getTasks][indexPath.row];
    return [current canDeleteTaskWithId:t.ID];
}

-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    if (editingStyle == UITableViewCellEditingStyleDelete){
        [TSNotifier showProgressOnView:cell withMessage:@"Deleting pict.."];
        PPTask* t = [current getTasks][indexPath.row];
        [pendingForDeletingTasks setValue:indexPath forKey:[NSString stringWithFormat:@"%d", t.ID]];
        [current deleteTaskWithId:t.ID];
        progressView = cell;
        
    }
}


// setting empty footer wil remove empty separators from table veiw
-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1.0f;
}
-(UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    PPTask* task = [current getTasks][indexPath.row];
    [[PPStorage sharedStorage] putValue:@(task.ID) forKey:STORAGE_VALUE_SELECTED_TASK_ID];
    NSString* destSeg= @"";
    
    if ([current.listName isEqualToString:TASK_LIST_AVAILABLE])
        destSeg = @"segUserArtistTask";
    else if ([current.listName isEqualToString:TASK_LIST_CURRENT])
        destSeg = @"segUserUserTask";
    else if ([current.listName isEqualToString:TASK_LIST_PENDING])
        destSeg = @"segUserArtistTask";
    
    [self performSegueWithIdentifier:destSeg sender:self];
}

// Loading More Picts ...

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (currentOffset > 0  && maximumOffset - currentOffset <= 50.0 && !isScrolling && ! loadingPage && !allPagesLoaded) {
        isScrolling = YES;
        [TSNotifier showProgressOnView:self.view withMessage:@"Loading more Picts.."];
        [current loadMoreTasks];
    }
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    isScrolling = NO;
}

@end
