//
//  PPContactSupportViewController.m
//  PictPerfect
//
//  Created by Adya on 10/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPContactSupportViewController.h"
#import "PPPrivacyPopupViewController.h"
#import "TSUtils.h"
#import "TSNotifier.h"
#import "PPLoginManager.h"

@interface PPContactSupportViewController (PPLoginManagerDelegate) <PPLoginCallback>

@end

@implementation PPContactSupportViewController

-(void) viewDidAppear:(BOOL)animated{
    [PPLoginManager sharedManager].delegate = self;
}

- (IBAction)finishClicked:(id)sender {
    NSString* msg = trim(self.tfMessage.text);
    self.tfMessage.text = msg;
    
    if (msg.length > 1){
        [TSNotifier showProgressOnView:self.view];
        [[PPLoginManager sharedManager] performContactSupportWithMessage:msg];
    }
    else{
        [TSNotifier notify:@"Message can't be empty."];
    }
}

-(void) onContactSupportResult:(BOOL)success orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [TSNotifier notify:@"Your Message has been sent. We will respond to you via Email."];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [TSNotifier notifyError:error];
    }
}
- (IBAction)showPrivacy:(id)sender {
    PPPrivacyPopupViewController* controller = [[PPPrivacyPopupViewController alloc] initWithNibName:@"PPPrivacyPopupViewController" bundle:nil];
    controller.delegate = self;
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationFade];
}

@end
