//
//  PPUserTaskPagerViewController.m
//  PictPerfect
//
//  Created by Adya on 12/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPUserTaskPagerViewController.h"
#import "PPStorage.h"
#import "PPLoginManager.h"
#import "PPTaskManager.h"
#import "TSNotifier.h"
#import "PPUserTaskDetailsViewController.h"
#import "PPUserTaskBidsListViewController.h"

@interface PPUserTaskPagerViewController (PPTaskManagerDelegate) <PPTaskManagerCallbacks>

@end

@implementation PPUserTaskPagerViewController{
    NSArray* controllers;
    NSArray* titles;
    PPTaskManager* taskManager;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    int taskId = [[[PPStorage sharedStorage] getValueForKey:STORAGE_VALUE_SELECTED_TASK_ID] intValue];
    taskManager = [[PPTaskManager alloc] initWithUser:[[PPLoginManager sharedManager] getUser]];
    taskManager.delegate = self;
    self.dataSource = self;
    self.delegate = self;
    [TSNotifier showProgressOnView:self.view withMessage:@"Loading Pict.."];
    [taskManager getTaskDetailsForTaskWithId:taskId];
}

-(void) firstInit{
    titles = @[@"Pict Details", @"Bids"];
    PPUserTaskDetailsViewController* details = [self.storyboard instantiateViewControllerWithIdentifier:@"UserTaskDetailsViewController"];
    PPUserTaskBidsListViewController* bids = [self.storyboard instantiateViewControllerWithIdentifier:@"UserTaskBidsListViewController"];
    
    details.taskManager = taskManager;
    bids.taskManager = taskManager;
    controllers = @[details, bids];
}

-(void) onTaskDetailsReceived:(PPTask *)task{
    [TSNotifier hideProgressOnView:self.view];
    if (controllers == nil){
        [self firstInit];
        [self reloadData];
    }
    for (id<PPTaskManagerCallbacks> c in controllers){ // forward callback to child controllers
        if ([c respondsToSelector:@selector(onTaskDetailsReceived:)]){
            [c onTaskDetailsReceived:task];
        }
    }
}

-(void) onBidSelected:(PPBid *)selectedBid{
    for (id<PPTaskManagerCallbacks> c in controllers){ // forward callback to child controllers
        if ([c respondsToSelector:@selector(onBidSelected:)]){
            [c onBidSelected:selectedBid];
        }
    }
}

-(void) onTaskCanceled{
    for (id<PPTaskManagerCallbacks> c in controllers){ // forward callback to child controllers
        if ([c respondsToSelector:@selector(onTaskCanceled)]){
            [c onTaskCanceled];
        }
    }
}

-(void) onTaskAccepted{
    for (id<PPTaskManagerCallbacks> c in controllers){ // forward callback to child controllers
        if ([c respondsToSelector:@selector(onTaskAccepted)]){
            [c onTaskAccepted];
        }
    }
}

-(void) onCommentPictureUploaded:(PPPicture *)picture{
    for (id<PPTaskManagerCallbacks> c in controllers){ // forward callback to child controllers
        if ([c respondsToSelector:@selector(onCommentPictureUploaded:)]){
            [c onCommentPictureUploaded:picture];
        }
    }
}

-(void) onCommentAdded{
    for (id<PPTaskManagerCallbacks> c in controllers){ // forward callback to child controllers
        if ([c respondsToSelector:@selector(onCommentAdded)]){
            [c onCommentAdded];
        }
    }
}

-(void) onCommentRemoved{
    for (id<PPTaskManagerCallbacks> c in controllers){ // forward callback to child controllers
        if ([c respondsToSelector:@selector(onCommentRemoved)]){
            [c onCommentRemoved];
        }
    }
}

-(void) onOperationFailed:(TaskOperation)operation withError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    for (id<PPTaskManagerCallbacks> c in controllers){ // forward callback to child controllers
        if ([c respondsToSelector:@selector(onOperationFailed:withError:)]){
            [c onOperationFailed:operation withError:error];
        }
    }
}

-(NSUInteger) numberOfTabsForViewPager:(ViewPagerController *)viewPager{
    return controllers.count;
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    UILabel *label = [UILabel new];
    label.text = titles[index];
    [label sizeToFit];
    label.textColor = STORAGE_COLORS_TITLE_COLOR;
    
    return label;
}
- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    
    return controllers[index];
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index {
    
    // Do something useful
    // Update taskView in conrtoller
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerIndicator:
            return STORAGE_COLORS_SECONDARY_COLOR;
        case ViewPagerTabsView:
            return STORAGE_COLORS_DARK_COLOR;
        default:
            return color;
    }
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    
    switch (option) {
        case ViewPagerOptionCenterCurrentTab:
            return 1.0;
        case ViewPagerOptionTabWidth:
            return self.view.frame.size.width/2;
        default:
            return value;
    }
}

@end
