//
//  PPChangePaswordViewController.h
//  PictPerfect
//
//  Created by Adya on 10/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPChangePasswordViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *tfOldPassword;
@property (strong, nonatomic) IBOutlet UITextField *tfNewPassword;
@property (strong, nonatomic) IBOutlet UITextField *tfConfirmNewPassword;

@end
