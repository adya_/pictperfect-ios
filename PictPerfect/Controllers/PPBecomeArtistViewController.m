//
//  PPBecomeArtistViewController.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPBecomeArtistViewController.h"
#import "PPLoginManager.h"
#import "TSUtils.h"
#import "TSNotifier.h"
#import "PPUser.h"

@interface PPBecomeArtistViewController (PPLoginManagerDelegate)<PPLoginCallback>

@end

@implementation PPBecomeArtistViewController

-(void) viewDidAppear:(BOOL)animated{
    [PPLoginManager sharedManager].delegate = self;
}

- (IBAction)sendClicked:(id)sender {
    NSString* email = trim(self.tfEmail.text);
    NSString* portfolio = trim(self.tfPortfolio.text);
    NSString* descr = trim(self.tfDescr.text);
    
    self.tfEmail.text = email;
    self.tfPortfolio.text = portfolio;
    self.tfDescr.text = descr;
    
    if (portfolio.length == 0)
        portfolio = nil; // reset this field if it is empty.
    PPLoginManager* manager = [PPLoginManager sharedManager];
    if ([manager isValidEmail:email] &&
        descr.length > 1 && (!portfolio ||
        [TSUtils isValidURL:portfolio])){
        [TSNotifier showProgressOnView:self.view];
        [manager performBecomeAnArtistWithPaypalEmail:email portfolio:portfolio andDescription:descr];
    }
    else{
        [TSNotifier notify:@"Some fields has incorrect values."];
    }
}

-(void) onBecomeAnArtistResult:(BOOL)success orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [TSNotifier notify:@"Your Request To Become An Artist Has Been Sent."];
        [[PPLoginManager sharedManager] getUser].role = PENDING;
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [TSNotifier notifyError:error];
    }
}


@end
