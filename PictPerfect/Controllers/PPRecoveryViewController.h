//
//  PPRecoveryViewController.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSViewController.h"

@interface PPRecoveryViewController : TSViewController
@property (strong, nonatomic) IBOutlet UITextField *tfEmail;

@end
