//
//  PPUserTaskDetailsViewController.h
//  PictPerfect
//
//  Created by Adya on 12/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PPTaskManager;

@interface PPUserTaskDetailsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property PPTaskManager* taskManager;
@end
