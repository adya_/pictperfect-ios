
#import <UIKit/UIKit.h>
#import "ViewPagerController.h"

@interface PPUserTaskPagerViewController : ViewPagerController<ViewPagerDataSource, ViewPagerDelegate>

@end
