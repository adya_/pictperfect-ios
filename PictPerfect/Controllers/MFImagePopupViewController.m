//
//  MFImagePopupViewController.m
//  Locksmith Resource
//
//  Created by Adya on 29/07/2014.
//  Copyright (c) 2014 Glar. All rights reserved.
//


#import "MFImagePopupViewController.h"
#import "PPRequestManager.h"
#import "TSNotifier.h"
#import "TSUtils.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"

@implementation MFImagePopupViewController{
    UIImage* img;
    NSString* url;
    BOOL _canSave;
}

@synthesize delegate;

-(void) viewDidLoad{
    [super viewDidLoad];
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close)];
    [self.view addGestureRecognizer:tap];
    [TSNotifier showProgressOnView:self.ivImage withMessage:@""];
    [[PPRequestManager sharedManager] requestImageFromURL:url forImageView:self.ivImage withPlaceholder:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        [TSNotifier hideProgressOnView:self.ivImage];
        self.bSave.hidden = !_canSave;
        img = image;
        self.ivImage.image = img;
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        [TSNotifier hideProgressOnView:self.ivImage];
        [self close];
    }];
}

-(void) close{
    [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

- (IBAction)bSave:(id)sender {
    [[ALAssetsLibrary new] saveImage:img toAlbum:APP_NAME withCompletionBlock:^(NSError *error) {
        [TSNotifier notify:@"Pict has been saved to Library in PictPerfecr aldum"];
    }];
}


-(void) setImageWithURL:(NSString *)_url canSave:(BOOL) canSave{
    url = _url;
    _canSave = canSave;

}

@end
