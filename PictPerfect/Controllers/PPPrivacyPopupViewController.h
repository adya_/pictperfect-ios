//
//  PPPrivacyPopupViewController.h
//  PictPerfect
//
//  Created by Adya on 28/04/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIViewController+MJPopupViewController.h"


@interface PPPrivacyPopupViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *bClose;
@property (strong, nonatomic) IBOutlet UITextView *tvPrivacy;

@property (nonatomic, assign) UIViewController* delegate;

@end
