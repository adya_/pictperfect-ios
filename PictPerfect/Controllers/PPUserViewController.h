//
//  PPArtistViewController.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSViewController.h"

@interface PPUserViewController : TSViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *lNoTasks;
@property (strong, nonatomic) IBOutlet UIToolbar *tbTabs;
@property (strong, nonatomic) IBOutlet UIButton *bAvailable;
@property (strong, nonatomic) IBOutlet UIButton *bPending;
@property (strong, nonatomic) IBOutlet UIButton *bYourPicts;

@end
