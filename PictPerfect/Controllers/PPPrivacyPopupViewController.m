
#import "PPPrivacyPopupViewController.h"

@implementation PPPrivacyPopupViewController

@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    NSString *html = [self readPrivacy];
    
    NSError *err = nil;
    self.tvPrivacy.attributedText =
    [[NSAttributedString alloc]
     initWithData: [html dataUsingEncoding:NSUTF8StringEncoding]
     options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
     documentAttributes: nil
     error: &err];
    if(err)
        NSLog(@"Unable to parse label text: %@", err);
}

-(NSString*) readPrivacy{
    NSString* content;
    NSString* path = [[NSBundle mainBundle] pathForResource:@"privacy" ofType:@"txt"];
    if(path)
    {
        content = [NSString stringWithContentsOfFile:path
                                            encoding:NSASCIIStringEncoding
                                               error:NULL];
    }
    return content;
}
- (IBAction)close:(id)sender {
    [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

@end
