<font color=\"#D1EBFF\">Terms &amp; Conditions</font>

<br/><br/><font color=\"#D1EBFF\"><b>INTRODUCTION TO THE PARTICIPATION AGREEMENT</b></font>  <br/>

The Terms and Conditions of this participation agreement control the use of PictPerfect.com (or \"Pict Perfect\") App (or the \"App\"). If you use the App, you agree to adhere to these terms and conditions. If this is not acceptable to you, then you may cease use of the App.

<br/><br/><font color=\"#D1EBFF\"><b>RIGHT TO CHANGE TERMS OF PARTICIPATION AGREEMENT</b></font>   <br/>

TASC, LLC (\"Pict Perfect\") has the right to change at any time any of the terms and conditions on the App without any liability, without any reason or notice to any User of the App or to you. When you visit or use the App, you agree to these terms and conditions that control its use.

<br/><br/><font color=\"#D1EBFF\"><b>WHO IS ELIGIBLE TO USE APP</b></font>  <br/>

Individuals and/or businesses (or \"Users\") are eligible to use the App if they meet the terms and conditions of this Participation agreement, and if their registrations are acceptable to Pict Perfect. Moreover, use of the App is limited to Users who can enter into a legally binding contract under applicable law that can be enforced. We reserve the right to refuse service to anyone and for any reason. Any User who is under the age of 18 is not allowed to use the App. Users must provide their real name, birthday and email address in order to register an Account. Users may not copy, reproduce, duplicate or sell any part of the App or the Content on the App or any access to the App. Pict Perfect may refuse service to anyone and for any reason.

<br/><br/><font color=\"#D1EBFF\"><b>YOUR USER NAME AND PASSWORD</b></font>  <br/>

You agree to protect your username and password information when you use Pict Perfect.com. You also agree to accept responsibility for all activities that occur under your username and password. We advise you to log out after each use to prevent others from acquiring your login information. You should secure your password as you are solely responsible and liable for any activities that occur under your password on this App. Pict Perfect is not responsible to verify the identity of a user that is logging in to the App with a password.

<br/><br/><font color=\"#D1EBFF\"><b>CONTENT</b></font>            <br/>

The Content (such as your picture, your comments and any other personal information) you submit, post, or display will be able to be viewed by Artists who are also users of the App. You should only provide Content that you are comfortable sharing with others under these Terms. All Content, whether publicly posted or privately transmitted, is the sole responsibility of the person who originated such Content. We do not monitor or control the Content posted via the App and, we cannot take responsibility for such Content. We do not endorse, support, represent or guarantee the completeness, truthfulness, accuracy, or reliability of any Content or communications posted via the App or endorse any opinions expressed via the App. You may find Content on the App that may be harmful, offensive, mislabeled, inaccurate or otherwise inappropriate, or in some cases, postings that have been mislabeled or otherwise deceptive, so your use of the App is \"As is\" and at your own risk. On the other hand, you are responsible for Content that you post, email or transmit and any consequences that arise as a result of your Content. Pict Perfect will not be liable under any circumstances and for any way, shape or form for any Content, including, but not limited to, any errors or omissions in the Content, or any loss or damage as a result of use of the Content whether posted, emailed, transmitted or made available via the App. Pict Perfect has the right to set limits on your Picture, Video or any file that you submit and is stored in our database at our own discretion and without notice to you. Pict Perfect has the right to also set limits on your use of the App. When you submit, post, display Content or make available on or through the App, you are granting us a perpetual, worldwide, royalty-free, non-exclusive, unrestricted, right to display, right to publish, along with the right to sublicense to other users or third parties, and irrevocable right to exercise all copyright, trademark rights to use, copy, reproduce, process, adapt, modify, transmit and distribute the Content without any compensation paid to you. You represent and warrant that you have all the power, authority and rights to grant us the license and/or rights to any Content that you submit.


<br/><br/><font color=\"#D1EBFF\"><b>POLICIES AND GUIDELINES</b></font>   <br/>

Pict Perfect provides a marketplace for our users to upload their pictures and for Artists (also users) to edit those pictures and hopefully improve them. You agree to follow our policies and guidelines when uploading pictures, making comments or when posting Content on the App:
   I) You agree not to register for more than one account.
   II) You may not post pictures or make comments that constitutes infringement on any third party product.
   III) You may not post pictures that are offensive, fraudulent or defamatory to others.
   IV) You may not post pictures that are stolen or illegal.
   V) You may not post any advertisement, commercial or promotional material.
   VI) You agree to honor your end of the contract whether you are posting pictures

<br/><br/><font color=\"#D1EBFF\"><b>RETURNS AND REFUNDS</b></font>      <br/>

Your use of the App and making payment on the App is at your own risk.  If you post a picture and do not agree with your picture becoming \"Perfect\", then you understand that there will not be any refunds in these circumstances.  What maybe perfect for the Artist may not be perfect for you, and vice versa.  Pict Perfect will only grant you a refund if the Artist has not met your deadline to make your Pict Perfect.  We ask that you contact Support and make this request before your improved Picture is provided to you by the Artist.

In all circumstances, we reserve the right to seek reimbursement from Artists at our discretion in the case where we need to reimburse a User (buyer) under the terms and conditions of this Participation agreement.

<br/><br/><font color=\"#D1EBFF\"><b>HOW TO RESOLVE DISPUTES</b></font>  <br/>

Pict Perfect is not involved in the transactions between users and artists, nor we do have authority in the actual transaction. In the case of a dispute between users and artists, Pict Perfect does not hold the role of an arbitrator nor do we represent either party to resolve these disputes. We may disclose the email address and/or phone number to allow each party to communicate with the other, and we recommend that both parties cooperate in resolving their disputes. Ultimately, it is up to users and artists to work together to resolve any issues. Users shall use the Pict Perfect App at their own risk.

<br/><br/><font color=\"#D1EBFF\"><b>PAYMENT FOR PICTURES</b></font>   <br/>

You must register for an account with Pict Perfect in order to post and upload pictures on PictPerfect.com. Payment method is via Paypal only. You may not use any other payment methods to pay for any Posts or Bids on the App.

<br/><br/><font color=\"#D1EBFF\"><b>FEES</b></font>          <br/>

PictPerfect.com charges fees for Artists as per the Artist agreement.

<br/><br/><font color=\"#D1EBFF\"><b>ILLEGAL ACTIVITY</b></font>   <br/>

Pict Perfect has the right but not the obligation to monitor any activity or content such as postings of pictures or comments on the App. Pict Perfect can investigate for violations of any inappropriate use of the App, including those of copyright infringement of other people\'s intellectual property rights. We reserve the right to remove any Content at any time, cancel any posting at any time, terminate any user access to the App at any time, or take any other action without prior notice and at our sole discretion. We may also terminate, change or suspend any part of the App or the entire App without any liability, without any reason or notice to any User of the App or to you. Users may not register under a false name. Users agree to list items and conduct transactions legally on the App. Any activities on the App that are considered fraudulent will be reported to authorities to prosecute the violators.

<br/><br/><font color=\"#D1EBFF\"><b>NO WARRANTY OR GUARANTEES TO USE THE APP</b></font> <br/>

The App is provided on an \"As Is Basis\" and \"As Available Basis\". Pict Perfect does not make any representations, conditions or warranties of any kind, express or implied which also includes without limitation. Pict Perfect will not be liable to you or to any other person, business or third party:
   * for any loss of use, economic loss of any kind, or any implied warranty arising from users who do or do not carry out their transactions as promised, do not make your  picture perfect, or that users do not breach their user agreements.
   * any content obtained from the App
   * unauthorized access, use or alteration of your transmissions or content,
   * any warranties express or implied, or representations of merchantability, fitness for a particular purpose performance, security and non-infringement.
   * for non-accessibility of the App, including App outage, availability, security or if the App falls short of your requirements or expectations.
   * for the deletion of, or the failure to store or to transmit, any Content and other communications maintained on the App. Any obligation, right, claim, remedy in tort and liability, whether or not they arise from the negligence of Pict Perfect, are all disclaimed by Pict Perfect to the full extent permitted by applicable law.

<br/><br/><font color=\"#D1EBFF\"><b>RELEASE FROM CLAIMS</b></font> <br/>

You agree to release Pict Perfect from any claims, transaction disputes among users and artists\' issues with Content, loss of production, loss of profits, damages, punitive damages or any kind of damage, that result from any disputes.

<br/><br/><font color=\"#D1EBFF\"><b>LIMITATION OF OUR LIABILITY</b></font> <br/>

Pict Perfect�s limitation of liability extends to penalties, judgments, monetary judgment awards including interest, attorney fees and any expenses that arise from any claim resulting from user disputes, user breach of agreement, return policy of products, infringement of copyrighted materials, infringement on any intellectual property or rights and failure to collect or to pay for taxes that result from an user transaction. You agree to indemnify, defend and hold Pict Perfect harmless anytime, anywhere and for any circumstance. Pict Perfect does not control and is not liable for the legality of the pictures that users post on the App as well as the accuracy, authenticity of the items and their description in the listings. Pict Perfect employees, contractors and/or affiliates may register for an account on Pict Perfect and utilize the services of the App at their own capacity. The participation is private and does not represent Pict Perfect in any way and it is subject to the same terms and conditions as any other user.

<br/><br/><font color=\"#D1EBFF\"><b>WHERE THE LAW APPLIES</b></font>  <br/>

The terms and conditions of this participation agreement are governed solely by the laws of Delaware, USA. Any dispute with Pict Perfect relating to these Terms and Conditions must be pronounced in a state court in the state of Georgia, and you agree to exclusive jurisdiction and venue in such courts. If any provision or section in the terms and conditions of the Participation Agreement is found to be unenforceable or invalid, then that provision or section must be separated from the rest of these Terms and Conditions, and the remaining terms and conditions of this Participation agreement shall remain in full force and effect. Any rights not expressly granted in these Terms and Conditions are reserved to Pict Perfect. These Terms and Conditions are subject to change without notice.

<br/><br/><font color=\"#D1EBFF\"><b>OWNERSHIP OF THE APP</b></font> <br/>

The App, Content of App, including any text, image or any other elements available are the property of Pict Perfect. Your use of the App does not transfer to you any ownership of the App or any other rights to its Content.

<br/><br/><font color=\"#D1EBFF\"><b>TRADENAME AND LOGO USE</b></font> <br/>

The Pict Perfect Logo and the Pict Perfect name are registered Trademarks. Any use of the trade names, trademarks, service marks and logos (or \"Marks\") that are displayed on the App is strictly prohibited. Any content that appears on the App shall not be interpreted as granting any license or right to use any Marks displayed on the App.

<br/><br/><font color=\"#D1EBFF\"><b>RIGHT TO MODIFY OR TERMINATE THE PARTICIPATION AGREEMENT</b></font>  <br/>

You may not modify or add to the terms and conditions of this Participation agreement or to any other policy that is listed on the App. Pict Perfect reserves the right to terminate this Participation Agreement at any time without any liability, without any reason or notice to any User of the App or to you. If any modification is not acceptable to you, then you may cease use of the App.