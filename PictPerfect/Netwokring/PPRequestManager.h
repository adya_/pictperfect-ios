//
//  PPRequestManager.h
//  PictPerfect
//
//  Created by Adya on 12/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "TSRequestManager.h"

@class PPPicture;

@interface PPRequestManager : TSRequestManager

+ (id) sharedManager;

@end

@interface PPRequestManager (Login)

-(void) requestLoginWithUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestGetRoleForUser:(NSString*)userToken withResponseCallback:(ObjectResponseCallback)callback;

-(void) requestForgotPasswordForUserWithEmail:(NSString*) userEmail withResponseCallback:(OperationResponseCallback) callback;

-(void) requestChangePasswordWithOldPassword:(NSString*) oldPassword toNewPassword:(NSString*) newPassword forUser:(NSString*) userToken withResponseCallback:(OperationResponseCallback) callback;

-(void) requestStopProposePasswordForUser:(NSString*) userToken withResponseCallback:(OperationResponseCallback) callback;

-(void) requestBecomeAnArtistForUser:(NSString*) userToken withPaypalEmail:(NSString*) paypalEmail description:(NSString*) descr andPortfolioSite:(NSString*) portfolio withResopnseCallback:(OperationResponseCallback) callback;

-(void) requestSignUpWithUsername:(NSString*) username password:(NSString*) password firstName:(NSString*) firstName andLastName:(NSString*) lastName withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestContactSupportWithMesage:(NSString*) message forUser:(NSString*) userToken withResponesCallback:(OperationResponseCallback) callback;
@end

@interface PPRequestManager (Tasks)

-(void) requestTaskDetailsWithID:(int) taskId forUser:(NSString*) userToken withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestGetTaskListWithName:(NSString*) listName skipFirst:(int) skip selectTotal:(int) total forUser:(NSString*) userToken withResponseCallback:(ArrayResponseCallback) callback;


-(void) requestAddTaskWithTitle:(NSString*) title description:(NSString*) descr imageId:(int) imageID deadline:(int) deadline forUser:(NSString*) userToken withResponseCallback:(OperationResponseCallback) callback;

-(void) requestMarkAsDoneTaskWithID:(int) taskID andArtistRating:(int) rating forUser:(NSString*) userToken withResponseCallback:(OperationResponseCallback) callback;

-(void) requestDeleteTaskWithID:(int) taskID forUser:(NSString*) userToken withResponseCallback:(OperationResponseCallback) callback;

-(void) requestCancelTaskWithID:(int) taskID forUser:(NSString*) userToken withResponseCallback:(OperationResponseCallback) callback;
@end

@interface PPRequestManager (Bids)
-(void) requestAddBidWithPrice:(double) bidPrice forTaskWithID:(int) taskID forUser:(NSString*) userToken withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestDeleteBidWithID:(int)bidID forUser:(NSString*) userToken wihtResponseCallback:(OperationResponseCallback) callback;

-(void) requestSelectBidWithID:(int) bidID andPayAuthorizationId:(NSString*)authorizationId forUser:(NSString*) userToken withResponseCallback:(OperationResponseCallback) callback;
@end

@interface PPRequestManager (Images)
-(void) requestAddImage:(UIImage*)image named:(NSString*) imageName andRotationAngle:(int) angle withResponseCallback:(ObjectResponseCallback)callback;
@end

@interface PPRequestManager (Comments)
-(void) requestAddCommentForTask:(int) taskID withComment:(NSString*) comment andImageId:(int) imageID forUser:(NSString*) userToken withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestAddCommentForTask:(int) taskID withComment:(NSString*) comment forUser:(NSString*) userToken withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestDeleteCommentWithID:(int) commentID forUser:(NSString*)userToken withResopnseCallback:(ObjectResponseCallback) callback;
@end