//
//  PPRequestManager.m
//  PictPerfect
//
//  Created by Adya on 12/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPRequestManager.h"
#import "PPUser.h"
#import "PPTask.h"
#import "PPBid.h"
#import "PPPicture.h"
#import "PPComment.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define REQUEST_LOGIN @"login/login"
#define REQUEST_GET_ROLE @"login/getRole"
#define REQUEST_FORGOT_PASSWORD @"login/forgotPSWD"
#define REQUEST_CHANGE_PASSWORD @"login/changePSWD"
#define REQUEST_STOP_PROPOSE_PASSWORD @"login/StopPSWDpropose"
#define REQUEST_BECOME_AN_ARTIST @"login/becomeAnArtist"
#define REQUEST_SIGN_UP @"login/signIn"
#define REQUEST_CONTACT_SUPPORT @"login/SendUserMessage"

#define REQUEST_TASK_DETAILS @"order/getDetails"
#define REQUEST_NAMED_TASK_LIST @"order/loadOrders"
#define REQUEST_ADD_TASK @"order/addOrder"
#define REQUEST_DELETE_TASK @"order/deleteOrder"
#define REQUEST_CANCEL_TASK @"order/cancelOrder"
#define REQUEST_MARK_TASK_AS_DONE @"order/setDone"

#define REQUEST_ADD_BID @"bid/addBid"
#define REQUEST_DELETE_BID @"bid/deleteBid"
#define REQUEST_SELECT_BID @"bid/bidSelect"

#define REQUEST_ADD_IMAGE @"image/addImage"
#define REQUEST_DELETE_IMAGE @"image/deleteImage"

#define REQUEST_ADD_COMMENT @"comment/addComment"
#define REQUEST_DELETE_COMMENT @"comment/deleteComment"

@implementation PPRequestManager

+ (id) sharedManager{
    static PPRequestManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

-(void) prepareManager{
    [self setAcceptTypes: @[@"text/html", @"text/json", @"multipart/form-data", @"application/json", @"application/x-www-from-urlencoded"]];
    [self setContentTypes: @[@"text/json", @"text/html", @"application/json", @"multipart/form-data"]];
}

@end

@implementation PPRequestManager (Login)

-(void) requestLoginWithUsername:(NSString *)username andPassword:(NSString *)password withResponseCallback:(ObjectResponseCallback)callback{
    NSDictionary* body = @{@"email":username,
                             @"password":password};
    [self POST:REQUEST_LOGIN withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[PPUser alloc] initWithJSON:[responseObject objectForKey:[self getResponseParamName]]], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestGetRoleForUser:(NSString *)userToken withResponseCallback:(ObjectResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    [self POST:REQUEST_GET_ROLE withBody:nil andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[PPUser alloc] initWithJSON:[responseObject objectForKey:[self getResponseParamName]]], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestForgotPasswordForUserWithEmail:(NSString *)userEmail withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* body = @{@"email":userEmail};
    [self POST:REQUEST_FORGOT_PASSWORD withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestChangePasswordWithOldPassword:(NSString*) oldPassword toNewPassword:(NSString *)newPassword forUser:(NSString *)userToken withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* body = @{@"password":newPassword,
                           @"oldPassword":oldPassword};
    NSDictionary* headers = @{@"access_token":userToken};
    [self POST:REQUEST_CHANGE_PASSWORD withBody:body andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestStopProposePasswordForUser:(NSString *)userToken withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    [self POST:REQUEST_STOP_PROPOSE_PASSWORD withBody:nil andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestBecomeAnArtistForUser:(NSString *)userToken withPaypalEmail:(NSString *)paypalEmail description:(NSString *)descr andPortfolioSite:(NSString *)portfolio withResopnseCallback:(OperationResponseCallback)callback{        NSDictionary* body = nil;
    if (portfolio){
        body = @{@"PayPallEmail":paypalEmail,
                           @"PortfolioSite":portfolio,
                           @"Description":descr};
    }
    else{
        body = @{@"PayPallEmail":paypalEmail,
                 @"Description":descr};
    }
    NSDictionary* headers = @{@"access_token":userToken};
    [self POST:REQUEST_BECOME_AN_ARTIST withBody:body andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestSignUpWithUsername:(NSString *)username password:(NSString *)password firstName:(NSString *)firstName andLastName:(NSString *)lastName withResponseCallback:(ObjectResponseCallback)callback{
    NSDictionary* body = @{@"email":username,
                             @"password":password,
                             @"first_name": firstName,
                             @"last_name": lastName};
    [self POST:REQUEST_SIGN_UP withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[PPUser alloc] initWithJSON:[responseObject objectForKey:[self getResponseParamName]]], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];

}

-(void) requestContactSupportWithMesage:(NSString *)message forUser:(NSString *)userToken withResponesCallback:(OperationResponseCallback)callback{
    NSDictionary* body = @{@"message":message};
    NSDictionary* headers = @{@"access_token":userToken};
    [self POST:REQUEST_CONTACT_SUPPORT withBody:body andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}
@end

@implementation PPRequestManager (Tasks)

-(void)requestTaskDetailsWithID:(int)taskId forUser:(NSString *)userToken withResponseCallback:(ObjectResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    NSString* urlParams = [NSString stringWithFormat:@"%@/%d", REQUEST_TASK_DETAILS, taskId];
    [self POST:urlParams withBody:nil andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[PPTask alloc] initWithJSON:[responseObject objectForKey:[self getResponseParamName]]], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestGetTaskListWithName:(NSString *)listName skipFirst:(int)skip selectTotal:(int)total forUser:(NSString*) userToken withResponseCallback:(ArrayResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    NSDictionary* body = @{@"type": listName};
    NSString* urlParams = [NSString stringWithFormat:@"%@?$skip=%d&$top=%d&$orderby=IdorderBy,CreateTime+desc", REQUEST_NAMED_TASK_LIST, skip, total];

    [self POST:urlParams withBody:body andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSMutableArray* tasks = [NSMutableArray arrayWithArray:responseObject];
            callback(YES, [NSArray arrayWithArray:tasks], nil);
     } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestAddTaskWithTitle:(NSString *)title description:(NSString *)descr imageId:(int)imageID deadline:(int) deadline forUser:(NSString *)userToken withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    NSDictionary* body = @{@"Task": title,
                           @"Descr":descr,
                           @"Imageid":@(imageID),
                           @"timeToDo":@(deadline)};
    [self POST:REQUEST_ADD_TASK withBody:body andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestMarkAsDoneTaskWithID:(int)taskID andArtistRating:(int)rating forUser:(NSString *)userToken withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    NSDictionary* body = @{@"vote": @(rating)};
    NSString* urlParams = [NSString stringWithFormat:@"%@/%d", REQUEST_MARK_TASK_AS_DONE, taskID];
    [self POST:urlParams withBody:body andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestDeleteTaskWithID:(int)taskID forUser:(NSString *)userToken withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    NSString* urlParams = [NSString stringWithFormat:@"%@/%d", REQUEST_DELETE_TASK, taskID];
    [self DELETE:urlParams withBody:nil andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestCancelTaskWithID:(int)taskID forUser:(NSString *)userToken withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    NSString* urlParams = [NSString stringWithFormat:@"%@/%d", REQUEST_CANCEL_TASK, taskID];
    [self POST:urlParams withBody:nil andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

@end

@implementation PPRequestManager (Bids)

-(void) requestAddBidWithPrice:(double)bidPrice forTaskWithID:(int)taskID forUser:(NSString *)userToken withResponseCallback:(ObjectResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    NSDictionary* body = @{@"price":@(bidPrice),
                           @"OrderId":@(taskID)};
    [self POST:REQUEST_ADD_BID withBody:body andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[PPBid alloc] initWithJSON:[responseObject objectForKey:[self getResponseParamName]]], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }

    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestDeleteBidWithID:(int)bidID forUser:(NSString *)userToken wihtResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    NSString* urlParams = [NSString stringWithFormat:@"%@/%d", REQUEST_DELETE_BID, bidID];
    [self DELETE:urlParams withBody:nil andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
        
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestSelectBidWithID:(int)bidID andPayAuthorizationId:(NSString*)authorizationId forUser:(NSString *)userToken withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    NSDictionary* body = @{@"transactionId":authorizationId};
    NSString* urlParams = [NSString stringWithFormat:@"%@/%d", REQUEST_SELECT_BID, bidID];
    [self POST:urlParams withBody:body andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
        
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}
@end

@implementation PPRequestManager (Images)

-(void) requestAddImage:(UIImage*)image named:(NSString*) imageName andRotationAngle:(int) angle withResponseCallback:(ObjectResponseCallback)callback{
    NSDictionary* headers = @{@"angle": [NSString stringWithFormat:@"%d", angle]};
    NSData* data = UIImageJPEGRepresentation(image, 1.0);
    [self POST:REQUEST_ADD_IMAGE withData:data named:[NSString stringWithFormat:@"%@%@", imageName, @".png"] withMimeType:@"image/png" andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[PPPicture alloc] initWithJSON:[responseObject objectForKey:[self getResponseParamName]]], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

@end

@implementation PPRequestManager (Comments)


-(void) requestAddCommentForTaskWithBody:(NSDictionary*) body forUser:(NSString*) userToken withResponseCallback:(ObjectResponseCallback) callback{
    NSDictionary* headers = @{@"access_token":userToken};
    [self POST:REQUEST_ADD_COMMENT withBody:body andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[PPTask alloc] initWithJSON:[responseObject objectForKey:[self getResponseParamName]]], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
        
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}


-(void) requestAddCommentForTask:(int) taskID withComment:(NSString*) comment andImageId:(int) imageId forUser:(NSString*) userToken withResponseCallback:(ObjectResponseCallback) callback{
    
    NSDictionary* body = @{@"Text": comment,
                           @"OrderId": @(taskID),
                           @"ImageId": @(imageId)};
    
    [self requestAddCommentForTaskWithBody:body forUser:userToken withResponseCallback:callback];
}

-(void) requestAddCommentForTask:(int) taskID withComment:(NSString*) comment forUser:(NSString*) userToken withResponseCallback:(ObjectResponseCallback) callback{
    NSDictionary* body = @{@"Text": comment,
                           @"OrderId": @(taskID)};
    
    [self requestAddCommentForTaskWithBody:body forUser:userToken withResponseCallback:callback];
}

-(void) requestDeleteCommentWithID:(int)commentID forUser:(NSString *)userToken withResopnseCallback:(ObjectResponseCallback)callback{
    NSDictionary* headers = @{@"access_token":userToken};
    NSString* urlParams = [NSString stringWithFormat:@"%@/%d", REQUEST_DELETE_COMMENT, commentID];
    [self DELETE:urlParams withBody:nil andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[PPTask alloc] initWithJSON:[responseObject objectForKey:[self getResponseParamName]]], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
        
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

@end

@implementation PPRequestManager (MustBeImplemented)

-(NSString*) getServerUrl{
    return @"http://pictperfect.com/api/";
}

-(NSString*) getFilesParamName{
    return @"Files";
}
-(NSString*) getResponseParamName{
    return @"Response";
}

-(NSString*) getErrorCodeParamName{
    return @"Error";
}

-(NSString*) getErrorMessageParamName{
    return @"ErrorMessage";
}

-(BOOL) isSuccessfulResponse:(NSDictionary *)response{
    id error = [response valueForKey:[self getErrorCodeParamName]];
    error = [[NSNull null] isEqual:error]? nil : error;
    return !error;
}


@end
