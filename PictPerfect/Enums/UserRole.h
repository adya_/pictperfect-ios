//
//  UserRole.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#ifndef PictPerfect_UserRole_h
#define PictPerfect_UserRole_h

typedef enum{
    USER = 1,
    PENDING, // IN_WORK
    ARTIST,
    BANNED
}UserRole;

#endif
