
// Designed to manage task lists and tasks.

// Has inner storage to cache loaded tasks. (Can be invalidated).

// Responsible for all operations with tasks.

#define TASK_LIST_CURRENT @"currentOrders"
#define TASK_LIST_AVAILABLE @"avalibleorders"
#define TASK_LIST_PENDING @"ordersInWork"

#define DEFAULT_PAGE_SIZE 20

@class PPTask;
@class PPUser;
@class TSError;

@protocol PPTaskManagerDelegate

-(void) onTaskList:(NSString*)listName loadedList:(NSArray*) taskList hasMore:(BOOL)hasMore;
-(void) onTaskDeleted:(int) taskId;
-(void) onOperationFailedWithError:(TSError*)error;
@end

@interface PPTaskListManager : NSObject

@property id<PPTaskManagerDelegate> delegate;
@property (readonly) NSString* listName;

-(id) initWithUser:(PPUser*) user andManagedTaskList:(NSString*) managedListName;

// load tasks from server and caches it to further use. If cache need to be updated call updateTasksInListNamed:;
-(void) loadMoreTasks;

// updates outdaetd list.
-(void) updateTasks;

// returns already loaded task list.
-(NSArray*) getTasks;


-(void) deleteTaskWithId:(int) taskId;
-(BOOL) canDeleteTaskWithId:(int) taskId;

@end
