
#import <Foundation/Foundation.h>

@class PPUser;
@class TSError;

@protocol PPLoginCallback <NSObject>
@optional -(void) onLoginResult:(BOOL) success withUser:(PPUser*) user orError:(TSError*) error;
@optional -(void) onPasswordResetResult:(BOOL) success orError:(TSError*) error;

@optional -(void) onPasswordChangedResult:(BOOL) success orError:(TSError*) error;
@optional -(void) onContactSupportResult:(BOOL) success orError:(TSError*) error;

@optional -(void) onBecomeAnArtistResult:(BOOL) success orError:(TSError*) error;
@end



// LoginManager manage login process

// 1. Set loginDelegate to handle auth process callbacks
// 2. Check if manager has some stored token.
// 2.1 If it has then call performLoginWithSavedUser to attempt to restore previous session if it wasn't ended with logout.
// 2.2 If it has not or prev step failed then call performLoginWihtUsername:andPassowrd: to perform login.

@interface PPLoginManager : NSObject


+ (PPLoginManager*) sharedManager;

@property id<PPLoginCallback> delegate;

-(PPUser*) getUser;

-(BOOL) hasStoredToken;

-(BOOL) isValidPassword:(NSString*)password;
-(BOOL) isValidEmail:(NSString*)email;
@end


@interface PPLoginManager (LoginProcess)

-(void) performLoginWithUsername:(NSString*) userName andPassword:(NSString*) password;

-(void) performLoginWithSavedUser;
-(void) logout;

@end

@interface PPLoginManager (RegistrationProcess)

-(void) performRegistrationWithUsername:(NSString*) username andPassword:(NSString*) password firstName:(NSString*) firstName andLastName:(NSString*) lastName;

@end

@interface PPLoginManager (ChangingPaswordProcess)

-(void) performPasswordRecoveryForUser:(NSString*)username;
-(void) performChangePasswordFromOldPassword:(NSString*) oldPassword toNewPassword:(NSString*) newPassword;

@end

@interface PPLoginManager (ConatctSupport)

-(void) performContactSupportWithMessage:(NSString*)message;

@end

@interface PPLoginManager (BecomeAnArtist)

-(void) performBecomeAnArtistWithPaypalEmail:(NSString*)email portfolio:(NSString*) portfolio andDescription:(NSString*) description;
@end