
#import "PPTaskListManager.h"
#import "PPRequestManager.h"
#import "PPTask.h"
#import "TSError.h"
#import "PPUser.h"

@implementation PPTaskListManager{
    PPUser* authUser;
    NSString* listName;
    NSMutableArray* taskList;
}

-(id) initWithUser:(PPUser *)user andManagedTaskList:(NSString *)managedListName{
    self = [super init];
    authUser = user;
    listName = managedListName;
    taskList = [NSMutableArray new];
    return self;
}

@synthesize delegate;

-(NSString*) listName{
    return listName;
}

-(PPUser*) authorizedUser{
    return authUser;
}

-(void) setAuthorizedUser:(PPUser *)authorizedUser{
    authUser = authorizedUser;
}

-(void) loadMoreTasks{
    NSInteger loadedAmount = taskList.count;
    [self loadMoreTasksTotal:(loadedAmount + DEFAULT_PAGE_SIZE)];
}

-(void) loadMoreTasksTotal:(int)amount{
    if (!authUser){
        if (delegate)
            [delegate onOperationFailedWithError:[self buildUserError]];
    }
    NSInteger loadedAmount = taskList.count;
    
    [[PPRequestManager sharedManager] requestGetTaskListWithName:listName skipFirst:loadedAmount selectTotal:amount forUser:authUser.token withResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        if (success){
            NSMutableArray* tasks = [NSMutableArray new];
            for (NSDictionary* task in array) {
                PPTask* t = [[PPTask alloc] initWithJSON:task];
                [tasks addObject:t];
                if (![taskList containsObject:t]){
                    [taskList addObject:t];
                }
            }
            
            if (delegate)
                [delegate onTaskList:listName loadedList:[NSArray arrayWithArray:taskList] hasMore:(array.count == DEFAULT_PAGE_SIZE)];
            
        }
        else{
            if (delegate)
                [delegate onOperationFailedWithError:error];
        }
    }];
 
}
-(void) updateTasks{
    int count = taskList.count;
    [taskList removeAllObjects];
    [self loadMoreTasksTotal:count];
}

-(NSArray*) getTasks{
    if (taskList.count == 0)
        return nil;
    else
        return [NSArray arrayWithArray:taskList];
}
-(TSError*) buildUserError{
       return [[TSError alloc] initWithCode:TS_ERROR_INTERNAL Title:@"User was not set."];
}

-(TSError*) buildTaskDeleteError{
    return [[TSError alloc] initWithCode:TS_ERROR_INTERNAL Title:@"Task can't be deleted."];
}


-(BOOL) canDeleteTaskWithId:(int)taskId{
    if (![TASK_LIST_CURRENT isEqualToString:listName]){
        return NO;
    }
    PPTask* task = [self getTaskWihtId:taskId];
    return (task && (task.status == CREATED || task.status == PENDING_ARTIST_SELECTION));
    
}

-(PPTask*) getTaskWihtId:(int) taskId{
    for (PPTask* t in taskList) {
        if (t.ID == taskId)
            return t;
    }
    return nil;
}

-(void) deleteTaskWithId:(int)taskId{
    if (![self canDeleteTaskWithId:taskId]){
        if (delegate)
            [delegate onOperationFailedWithError:[self buildTaskDeleteError]];
        
    }
    [[PPRequestManager sharedManager] requestDeleteTaskWithID:taskId forUser:authUser.token withResponseCallback:^(BOOL success, TSError *error) {
        if (success){
            [taskList removeObject:[self getTaskWihtId:taskId]];
            if (delegate){
                [delegate onTaskDeleted:taskId];
            }
            
        }
        else{
            if(delegate)
                [delegate onOperationFailedWithError:error];
        }
    }];
}

@end
