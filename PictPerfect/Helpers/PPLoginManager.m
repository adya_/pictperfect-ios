#import "PPLoginManager.h"

#import "PPRequestManager.h"
#import "PPUser.h"
#import "TSError.h"

#define USER_TOKEN @"userToken"
#define USER_EMAIL @"userEmail"

@implementation PPLoginManager{
    PPUser* user; // logged in user
}

@synthesize delegate;

+ (PPLoginManager*) sharedManager{
    static PPLoginManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

-(id) init{
    self = [super init];
    [self loadUserData];
    return self;
}
-(void) clearUserData{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:USER_TOKEN];
    [prefs synchronize];
}

-(void) saveUserData{
    if (!user) return;
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:user.email forKey:USER_EMAIL];
    [prefs setObject:user.token forKey:USER_TOKEN];
    [prefs synchronize];
}

-(void) loadUserData{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    NSString* email = [prefs objectForKey:USER_EMAIL];
    NSString* token = [prefs objectForKey:USER_TOKEN];
    user =  [[PPUser alloc] initWithUsername:email andToken:token];
}


-(PPUser*) getUser{
    return user;
}

-(BOOL) hasStoredToken{
    return ((user != nil) && (user.token != nil));
}

-(BOOL) isValidEmail:(NSString *)email{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}
-(BOOL) isValidPassword:(NSString *)password{
    password = [password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return password != nil && password.length >= 4;
}

@end

@implementation PPLoginManager (LoginProcess)

-(void) performLoginWithUsername:(NSString *)userName andPassword:(NSString *)password{
    [[PPRequestManager sharedManager] requestLoginWithUsername:userName andPassword:password withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if (success){
            PPUser* authorizedUser = (PPUser*)object;
            user = [[PPUser alloc] initWithUsername:userName andToken:authorizedUser.token];
            user.role = authorizedUser.role;
            [self saveUserData];
        }
        else{
            user = nil;
        }
        if ([delegate respondsToSelector:@selector(onLoginResult:withUser:orError:)])
            [delegate onLoginResult:success withUser:user orError:error];
    }];
}

-(void) performLoginWithSavedUser{
    if (![self hasStoredToken]){
        if ([delegate respondsToSelector:@selector(onLoginResult:withUser:orError:)])
            [delegate onLoginResult:NO withUser:nil orError:nil];
        return;
    }
    [[PPRequestManager sharedManager] requestGetRoleForUser:user.token withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if (success){
            user.role = ((PPUser*)object).role;
        }
        if ([delegate respondsToSelector:@selector(onLoginResult:withUser:orError:)])
            [delegate onLoginResult:success withUser:user orError:error];
    }];
}

-(void) logout{
    user = [[PPUser alloc] initWithUsername:user.email andToken:nil];
    [self clearUserData];
}

@end


@implementation PPLoginManager (RegistrationProcess)

-(void) performRegistrationWithUsername:(NSString *)username andPassword:(NSString *)password firstName:(NSString *)firstName andLastName:(NSString *)lastName{
    [[PPRequestManager sharedManager] requestSignUpWithUsername:username password:password firstName:firstName andLastName:lastName withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if (success){
            PPUser* authorizedUser = (PPUser*)object;
            user = [[PPUser alloc] initWithUsername:username andToken:authorizedUser.token];
            [self saveUserData];
        }
        else{
            user = nil;
        }
        if ([delegate respondsToSelector:@selector(onLoginResult:withUser:orError:)])
            [delegate onLoginResult:success withUser:user orError:error];
     }];
}

@end

@implementation PPLoginManager (ChangingPaswordProcess)

-(void) performPasswordRecoveryForUser:(NSString *)username{
    [[PPRequestManager sharedManager] requestForgotPasswordForUserWithEmail:username withResponseCallback:^(BOOL success, TSError *error) {
        if ([delegate respondsToSelector:@selector(onPasswordResetResult:orError:)])
            [delegate onPasswordResetResult:success orError:error];
    }];
}

-(void) performChangePasswordFromOldPassword:(NSString*) oldPassword toNewPassword:(NSString *)newPassword{
    [[PPRequestManager sharedManager] requestChangePasswordWithOldPassword:(NSString*) oldPassword toNewPassword:newPassword forUser:user.token withResponseCallback:^(BOOL success, TSError *error) {
        if ([delegate respondsToSelector:@selector(onPasswordChangedResult:orError:)])
            [delegate onPasswordChangedResult:success orError:error];
    }];
            
}

@end

@implementation PPLoginManager (ConatctSupport)

-(void) performContactSupportWithMessage:(NSString *)message{
    [[PPRequestManager sharedManager] requestContactSupportWithMesage:message forUser:user.token withResponesCallback:^(BOOL success, TSError *error) {
        if ([delegate respondsToSelector:@selector(onContactSupportResult:orError:)])
            [delegate onContactSupportResult:success orError:error];
    }];
}

@end

@implementation PPLoginManager (BecomeAnArtist)

-(void) performBecomeAnArtistWithPaypalEmail:(NSString *)email portfolio:(NSString *)portfolio andDescription:(NSString *)description{
    [[PPRequestManager sharedManager] requestBecomeAnArtistForUser:user.token withPaypalEmail:email description:description andPortfolioSite:portfolio withResopnseCallback:^(BOOL success, TSError *error) {
        if ([delegate respondsToSelector:@selector(onBecomeAnArtistResult:orError:)]){
            [delegate onBecomeAnArtistResult:success orError:error];
        }
    }];
}

@end