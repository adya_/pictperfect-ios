//
//  PPMenuManager.m
//  PictPerfect
//
//  Created by Adya on 03/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPMenuManager.h"
#import "REMenu.h"
#import "PPLoginManager.h"
#import "PPUser.h"
#import "PPStorage.h"
@implementation PPMenuManager{
    UIViewController* managedViewController;
    REMenu* menu;
}

-(id) initWithViewController:(UIViewController *)viewController{
    self = [super init];
    managedViewController = viewController;
    [self initMenu];
    return self;
}

-(void) initMenu{
    PPUser* user = [[PPLoginManager sharedManager] getUser];
    BOOL canBecomeArtist = user.role == USER;
      REMenuItem* item1 = [[REMenuItem alloc] initWithTitle:@"New Pict" image:nil highlightedImage:nil action:^(REMenuItem *item) {
            [managedViewController performSegueWithIdentifier:@"segUserNewTask" sender:managedViewController];
    }];
    REMenuItem* item2 = nil;
    if (canBecomeArtist){
        item2 = [[REMenuItem alloc] initWithTitle:@"Become an Artist" image:nil highlightedImage:nil action:^(REMenuItem *item) {
            [managedViewController performSegueWithIdentifier:@"segUserBecomeArtist" sender:managedViewController];
        }];
    }
    
    REMenuItem* item3 = [[REMenuItem alloc] initWithTitle:@"Change Password" image:nil highlightedImage:nil action:^(REMenuItem *item) {
        [managedViewController performSegueWithIdentifier:@"segUserChangePassword" sender:managedViewController];
    }];
    
    REMenuItem* item4 = [[REMenuItem alloc] initWithTitle:@"Contact Support" image:nil highlightedImage:nil action:^(REMenuItem *item) {
        [managedViewController performSegueWithIdentifier:@"segUserContactSupport" sender:managedViewController];
    }];
    
    REMenuItem* item5 = [[REMenuItem alloc] initWithTitle:@"Logout" image:nil highlightedImage:nil action:^(REMenuItem *item) {
        [[PPLoginManager  sharedManager] logout];
        [managedViewController.navigationController popToRootViewControllerAnimated:YES];
    }];
    if (canBecomeArtist)
        menu = [[REMenu alloc] initWithItems:@[item1, item2, item3, item4, item5]];
    else
        menu = [[REMenu alloc] initWithItems:@[item1, item3, item4, item5]];
    [menu setTextColor:STORAGE_COLORS_DARK_TITLE_COLOR];
}

-(void) toggleMenu{
    if (menu.isOpen)
        [menu close];
    else
        [menu showFromNavigationController:managedViewController.navigationController];
}
@end
