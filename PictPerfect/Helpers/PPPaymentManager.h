
#import <Foundation/Foundation.h>



typedef enum{PAYMENT_TEMP = -1, PAYMENT_SUCCESS, PAYMENT_ERROR} PPPaymentStatus;

@interface PPPaymentResult : NSObject
@property (readonly) NSString* authorizationId;
@property (readonly) NSString* transactionId;
@property (readonly) PPPaymentStatus status;
@property (readonly) int taskId;
@property (readonly) int bidId;

-(id) initWithStatus:(PPPaymentStatus)status withAuthorizationId:(NSString*)authId andTransactionId:(NSString*)transId forBid:(int) bidId inTask:(int) taskId;
@end

@protocol PPPaymentManagerDelegate <NSObject>

-(void) onPaymentCompleted:(PPPaymentResult*) result;
-(void) onPaymentCanceled;
@end

@interface PPPaymentManager : NSObject

+(PPPaymentManager*) sharedManager;

@property id<PPPaymentManagerDelegate> delegate;

@property UIViewController* parentViewController;

/// Prepares payment operation and displays payment view conrtoller to furhter payment preparation.
/// Returns payment validation results.
-(BOOL) pay:(double) amount withPurpose:(NSString*) description forBid:(int) bidId inTask:(int) taskId;

-(void) savePayment:(PPPaymentResult*) payment;
-(void) removePayment:(PPPaymentResult*) payment;
-(NSDictionary*) savedPayments;
-(PPPaymentResult*) savedPaymentForTask:(int)taskId;

@end
