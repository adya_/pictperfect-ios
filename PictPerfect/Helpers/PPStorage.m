//
//  PPStorage.m
//  PictPerfect
//
//  Created by Adya on 03/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPStorage.h"

@implementation PPStorage


+ (PPStorage*) sharedStorage{
    static PPStorage* storage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{storage = [[self alloc] init];});
    return storage;
}

-(id) init{
    self = [super init];
    [self loadStorage];
    return self;
}

-(void) loadStorage{
//    [self putValue:colorRGB(192, 35, 13) forKey:STORAGE_COLORS_ERROR_COLOR];
//    
//    [self putValue:colorRGB(0, 0, 0) forKey:STORAGE_COLORS_DEFAULT_TASK_COLOR];
//    [self putValue:colorRGB(212, 159, 35) forKey:STORAGE_COLORS_PENDING_TASK];
//    [self putValue:colorRGB(85, 169, 0) forKey:STORAGE_COLORS_WORKING_TASK];
//    [self putValue:colorRGB(72, 106, 143) forKey:STORAGE_COLORS_DONE_TASK];
//    
//    [self putValue:colorRGB(192, 35, 13) forKey:STORAGE_COLORS_DEADLINE_REACHED];
//    [self putValue:colorRGB(212, 148, 100) forKey:STORAGE_COLORS_DEADLINE_RUNS_OUT];
//    
//    [self putValue:colorRGB(85, 169, 0) forKey:STORAGE_COLORS_COMMENT_ARTIST];
//    [self putValue:colorRGB(212, 159, 35) forKey:STORAGE_COLORS_COMMENT_USER];
//    [self putValue:colorARGB(136, 88, 159, 199) forKey:STORAGE_COLORS_NEW_COMMENT_BG];
//    
//    [self putValue:colorRGB(64, 100, 130) forKey:STORAGE_COLORS_MAIN_COLOR];
//    [self putValue:colorRGB(209, 235, 255) forKey:STORAGE_COLORS_TITLE_COLOR];
//    [self putValue:colorRGB(95, 140, 190) forKey:STORAGE_COLORS_SECONDARY_COLOR];
//    [self putValue:colorRGB(45, 64, 91) forKey:STORAGE_COLORS_BORDER_COLOR];
//    [self putValue:colorRGB(64, 64, 64) forKey:STORAGE_COLORS_DARK_COLOR];
    
}
@end
