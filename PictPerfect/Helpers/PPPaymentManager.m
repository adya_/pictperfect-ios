
#import "PPPaymentManager.h"
#import "PayPalMobile.h"

#define CLIENT_ID_PRODUCTION @""
#define CLIENT_ID_SANDBOX @"AXY27xCn9q8_g6B9_t68hgTSiBmKnNTIi2M8yKCUGbQmE12PTsMR6A3WdVW4"

#define PAYMENT_KEY @"PictPerfect-PayKey"

@interface PPPaymentManager (PayPalDelegate)<PayPalPaymentDelegate>

@end

@implementation PPPaymentResult

@synthesize authorizationId = _authorizationId;
@synthesize transactionId = _transactionId;
@synthesize status = _status;
@synthesize taskId = _taskId;
@synthesize bidId = _bidId;

-(id) initWithStatus:(PPPaymentStatus)status withAuthorizationId:(NSString *)authId andTransactionId:(NSString *)transId forBid:(int)bidId inTask:(int)taskId{
    self = [self init];
    _authorizationId = authId;
    _transactionId = transId;
    _bidId = bidId;
    _taskId = taskId;
    _status = status;
    return self;
}

@end

@implementation PPPaymentManager{
    PayPalConfiguration* configuration;
    PPPaymentResult* currentPayment;
}

@synthesize parentViewController;


+ (PPPaymentManager*) sharedManager{
    static PPPaymentManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}


-(id) init{
    self = [super init];
    [PayPalMobile initializeWithClientIdsForEnvironments:@{
                PayPalEnvironmentProduction : CLIENT_ID_PRODUCTION,
                PayPalEnvironmentSandbox : CLIENT_ID_SANDBOX
    }];
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
    
    configuration = [PayPalConfiguration new];
    configuration.forceDefaultsInSandbox = YES;
    configuration.sandboxUserPin = @"test.email@email.com";
    configuration.sandboxUserPassword = @"test.email@email.com";
    return self;
}

-(NSString*) buildPrefsKeyForTask:(int) taskId andBid:(int) bidId{
    return [NSString stringWithFormat:@"%@_%d_%d", PAYMENT_KEY, taskId, bidId];
}

-(NSString*) buildPrefsValueForAuthId:(NSString*) authId andTransId:(NSString*) transId{
    return [NSString stringWithFormat:@"%@_%@", authId, transId];
}

-(void) savePayment:(PPPaymentResult*) payment{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:[self buildPrefsValueForAuthId:payment.authorizationId andTransId:payment.transactionId] forKey:[self buildPrefsKeyForTask:payment.taskId andBid:payment.bidId]];
    [prefs synchronize];
}

-(void) removePayment:(PPPaymentResult*) payment{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:[self buildPrefsKeyForTask:payment.taskId andBid:payment.bidId]];
    [prefs synchronize];
}

-(NSDictionary*) savedPayments{
    NSMutableDictionary* results = [NSMutableDictionary new];
    NSDictionary* stored = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    [stored enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        // since we using global storage, ensure that processing key/value pair is corresponds to payment. (so both key/value are NSString and key starts with our unique prefix)
        if (![key isKindOfClass:[NSString class]] || ![obj isKindOfClass:[NSString class]])
            return;
        
        NSArray* keys = [key componentsSeparatedByString:@"_"];
        NSArray* values = [obj componentsSeparatedByString:@"_"];
        
        if (keys.count != 3 || ![keys[0] isEqualToString:PAYMENT_KEY])
            return;

        int taskId = [keys[1] intValue];
        int bidId = [keys[2] intValue];
        NSString* authId = values[0];
        NSString* transId = values[1];
        PPPaymentResult* res = [[PPPaymentResult alloc] initWithStatus:PAYMENT_SUCCESS withAuthorizationId:authId andTransactionId:transId forBid:bidId inTask:taskId];
        [results setObject:res forKey:[NSString stringWithFormat:@"%d", res.taskId]];
    }];
    return [results copy];
}

-(PPPaymentResult*) savedPaymentForTask:(int)targetTaskId{
    NSDictionary* stored = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    __block PPPaymentResult* res = nil;
    [stored enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        // since we using global storage, ensure that processing key/value pair is corresponds to payment. (so both key/value are NSString and key starts with our unique prefix)
        if (![key isKindOfClass:[NSString class]] || ![obj isKindOfClass:[NSString class]])
            return;
        
        NSArray* keys = [key componentsSeparatedByString:@"_"];
        NSArray* values = [obj componentsSeparatedByString:@"_"];
        
        if (keys.count != 3 || ![keys[0] isEqualToString:PAYMENT_KEY])
            return;
        int taskId = [keys[1] intValue];
        if (taskId == targetTaskId) {
            int bidId = [keys[2] intValue];
            NSString* authId = values[0];
            NSString* transId = values[1];
            res = [[PPPaymentResult alloc] initWithStatus:PAYMENT_SUCCESS withAuthorizationId:authId andTransactionId:transId forBid:bidId inTask:taskId];
            *stop = YES;
        }
    }];
    return res;
}

-(BOOL) pay:(double)amount withPurpose:(NSString *)description forBid:(int)bidId inTask:(int)taskId{
    PayPalPayment* payment = [PayPalPayment new];
    payment.currencyCode = @"USD";
    payment.amount = [[NSDecimalNumber alloc] initWithDouble:amount];
    payment.shortDescription = description;
    payment.intent = PayPalPaymentIntentAuthorize;
    currentPayment = nil;
    if (payment.processable){
        currentPayment = [[PPPaymentResult alloc] initWithStatus:PAYMENT_TEMP withAuthorizationId:nil andTransactionId:nil forBid:bidId inTask:taskId];
        
        PayPalPaymentViewController* paymentViewConrtoller = [[PayPalPaymentViewController alloc] initWithPayment:payment configuration:configuration delegate:self];
        [parentViewController presentViewController:paymentViewConrtoller animated:YES completion:nil];
    }
    return payment.processable;
}

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment {
    NSDictionary* confirmation = [completedPayment.confirmation valueForKey:@"response"];
    BOOL state = [[confirmation valueForKey:@"state"] isEqualToString:@"approved"];
    NSString* authId = [confirmation valueForKey:@"authorization_id"];
    NSString* transId = [confirmation valueForKey:@"id"];
    
    PPPaymentResult* result = [[PPPaymentResult alloc] initWithStatus:(state?PAYMENT_SUCCESS:PAYMENT_ERROR) withAuthorizationId:authId andTransactionId:transId forBid:currentPayment.bidId inTask:currentPayment.taskId];
    currentPayment = nil;
    if ([self.delegate respondsToSelector:@selector(onPaymentCompleted:)])
        [self.delegate onPaymentCompleted:result];
    
    [parentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    if ([self.delegate respondsToSelector:@selector(onPaymentCanceled)])
        [self.delegate onPaymentCanceled];

    [parentViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
