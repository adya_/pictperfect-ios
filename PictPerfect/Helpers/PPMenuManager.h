//
//  PPMenuManager.h
//  PictPerfect
//
//  Created by Adya on 03/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPMenuManager : NSObject

-(id) initWithViewController:(UIViewController*) viewController;

-(void) toggleMenu;
@end
