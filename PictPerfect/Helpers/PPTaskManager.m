#import "PPTaskManager.h"

#import "PPRequestManager.h"

#import "PPTask.h"
#import "PPBid.h"
#import "PPComment.h"
#import "PPPicture.h"
#import "TSError.h"
#import "PPUser.h"
#import "PPArtist.h"
#import "TSUtils.h"

@implementation PPTaskManager{
    NSString* userToken;
}

@synthesize task;
@synthesize confirmedBid;
@synthesize placedBid;

-(NSArray*) comments{
    return [NSArray arrayWithArray:task.comments];
}

-(NSArray*) bids{
    return [NSArray arrayWithArray:task.bids];
}

-(int) bidsCount{
    return task.bids.count;
}

-(int) commentsCount{
    return task.comments.count;
}

-(BOOL) isReady{
    return userToken != nil && task != nil;
}

@synthesize isArtist;

-(BOOL) isPendingArtist{
    return task.status == PENDING_ARTIST_SELECTION;
}
-(BOOL) isTaken{
    return (task.status != UNDEFINED &&
            task.status != CREATED &&
            task.status != PENDING_ARTIST_SELECTION);
}

-(BOOL) isProgressed{
    return (task.status == PENDING_FOR_REVIEW ||
            task.status == PENDING);
}

-(BOOL) isFinished{
    return task.status == DONE;
}

-(BOOL) isOwnTask{
    return task.author == nil;
}

-(BOOL) isDeadlineReached{
    return task.timeLeft < 0;
}

-(BOOL) hasArtistPicture{
    PPComment* c = [self lookForCommentWithPredicate:^BOOL(PPComment *candidate) {
        return ((candidate.isArtist) && (candidate.picture.isValid));
    }];
    return c != nil;
}

-(PPComment*) commentAtIndex:(int)index{
    if (index < 0 || index >= task.comments.count)
        return nil;
    else
        return task.comments[index];
}

-(PPBid*) bidAtIndex:(int)index{
    if (index < 0 || index >= task.bids.count)
        return nil;
    else
        return task.bids[index];
}


-(BOOL) ensureIsReady{
    if (!self.isReady){
        if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)])
            [self.delegate onOperationFailed:TASK_OP_NONE withError:[[TSError alloc] initWithCode:TS_ERROR_INTERNAL Title:@"PPTaskManager is not ready to do requests."]];
    }
    return self.isReady;
    
}

-(id) initWithUser:(PPUser*)user{
    self = [self init];
    userToken = user.token;
    isArtist = (user.role == ARTIST);
    return self;
}

-(PPBid*) lookForBidWithPredicate:(BOOL(^)(PPBid* candidate)) predicate{
    for (PPBid* b in task.bids) {
        if (predicate && predicate(b)){
            return b;
        }
    }
    return nil;
}

-(PPComment*) lookForCommentWithPredicate:(BOOL(^)(PPComment* candidate)) predicate{
    for (PPComment* c in task.comments) {
        if (predicate && predicate(c)){
            return c;
        }
    }
    return nil;
}


-(void) getTaskDetailsForTaskWithId:(int) taskId{
    [[PPRequestManager sharedManager] requestTaskDetailsWithID:taskId forUser:userToken withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if (success){
            task = (PPTask*)object;
            confirmedBid = [self lookForBidWithPredicate:^BOOL(PPBid *candidate) {
                return candidate.isConfirmed;
            }];
            
            if (self.isArtist && task.bids.count > 0){
                placedBid = task.bids[0];
            }
            
            if([self.delegate respondsToSelector:@selector(onTaskDetailsReceived:)]){
                [self.delegate onTaskDetailsReceived:task];
            }
        }
        else{
            if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)])
                [self.delegate onOperationFailed:TASK_OP_TASK_LOAD_DETAILS withError:error];
        }
    }];
}

-(void) acceptTaskWithArtistRating:(int)rating{
    if (![self ensureIsReady]) return;
    
    [[PPRequestManager sharedManager] requestMarkAsDoneTaskWithID:task.ID andArtistRating:rating forUser:userToken withResponseCallback:^(BOOL success, TSError *error) {
        if (success){
            task.status = DONE;
            if ([self.delegate respondsToSelector:@selector(onTaskAccepted)])
                [self.delegate onTaskAccepted];
        }
        else{
            if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)]){
                [self.delegate onOperationFailed:TASK_OP_TASK_ACCEPT withError:error];
            }
        }
    }];
}

-(void) cancelTask{
    [[PPRequestManager sharedManager] requestCancelTaskWithID:task.ID forUser:userToken withResponseCallback:^(BOOL success, TSError *error) {
        if (success){
            confirmedBid = nil;
            task.status = (self.bidsCount == 1 ? CREATED : PENDING_ARTIST_SELECTION);
            if ([self.delegate respondsToSelector:@selector(onTaskCanceled)])
                [self.delegate onTaskCanceled];
        }
        else{
            if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)]){
                [self.delegate onOperationFailed:TASK_OP_TASK_CANCEL withError:error];
            }
        }

    }];
}

-(void) selectBidWithId:(int)bidId andPayAuthId:(NSString *)authId{
    if (![self ensureIsReady]) return;
    if (confirmedBid != nil){
        if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)])
            [self.delegate onOperationFailed:TASK_OP_BID_SELECT withError:[[TSError alloc] initWithCode:TS_ERROR_INTERNAL Title:@"There is already confirmed Bid."]];
        return;
    }
    [[PPRequestManager sharedManager] requestSelectBidWithID:bidId andPayAuthorizationId:authId forUser:userToken withResponseCallback:^(BOOL success, TSError *error) {
        if (success){
            confirmedBid = [self lookForBidWithPredicate:^BOOL(PPBid *candidate) {
                return (candidate.ID == bidId);
            }];
            task.status = WORK_STARTED;
            if ([self.delegate respondsToSelector:@selector(onBidSelected:)])
                [self.delegate onBidSelected:confirmedBid];
        }else{
            if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)]){
                [self.delegate onOperationFailed:TASK_OP_BID_SELECT withError:error];
            }
        }
    }];
}

-(void) placeBidWithPrice:(double)price{
    if (![self ensureIsReady]) return;
    
    [[PPRequestManager sharedManager] requestAddBidWithPrice:price forTaskWithID:task.ID forUser:userToken withResponseCallback:^(BOOL success, NSObject* object, TSError *error) {
        if (success){
            placedBid = (PPBid*)object;
            if ([self.delegate respondsToSelector:@selector(onBidPlaced:)]){
                [self.delegate onBidPlaced:placedBid];
            }
        }
        else{
            if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)]){
                [self.delegate onOperationFailed:TASK_OP_BID_PLACE withError:error];
            }
        }
    }];
}

-(void) cancelBid{
    if (![self ensureIsReady]) return;
    
    if (placedBid == nil){
        if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)])
            [self.delegate onOperationFailed:TASK_OP_NONE withError:[[TSError alloc] initWithCode:TS_ERROR_INTERNAL Title:@"There is no placed bid to cancel."]];
        return;
    }
    
    [[PPRequestManager sharedManager] requestDeleteBidWithID:placedBid.ID forUser:userToken wihtResponseCallback:^(BOOL success, TSError *error) {
        if (success){
            placedBid = nil;
            if ([self.delegate respondsToSelector:@selector(onBidCanceled)]){
                [self.delegate onBidCanceled];
            }
        }
        else{
            if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)]){
                [self.delegate onOperationFailed:TASK_OP_BID_CANCEL withError:error];
            }
        }
    }];
}

-(void) addCommentWithMeassage:(NSString *)message andPicture:(UIImage *)picture{
    if (![self ensureIsReady]) return;
    
    if (picture == nil){
        [[PPRequestManager sharedManager] requestAddCommentForTask:task.ID withComment:message forUser:userToken withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
            if (success){
                [task.comments removeAllObjects];
                [task.comments addObjectsFromArray:((PPTask*)object).comments];
                if (self.isTaken){
                    if (self.commentsCount == 0){
                        task.status = WORK_STARTED;
                    }
                    else{
                        PPComment* last = [task.comments objectAtIndex:0];
                        if (isArtist)
                            task.status = (last.isArtist ? PENDING_FOR_REVIEW : PENDING);
                        else
                            task.status = (last.isArtist ? PENDING : PENDING_FOR_REVIEW);
                    }
                }
                if ([self.delegate respondsToSelector:@selector(onCommentAdded)])
                    [self.delegate onCommentAdded];
            }
            else{
                if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)]){
                    [self.delegate onOperationFailed:TASK_OP_COMMENT_ADD withError:error];
                }
            }
            
        }];
    }
    else{
        [[PPRequestManager sharedManager] requestAddImage:picture named:APP_NAME andRotationAngle:0 withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
            if (success){
                PPPicture* pict = (PPPicture*)object;
                if ([self.delegate respondsToSelector:@selector(onCommentPictureUploaded:)])
                    [self.delegate onCommentPictureUploaded:pict];
                [[PPRequestManager sharedManager] requestAddCommentForTask:task.ID withComment:message andImageId:pict.ID forUser:userToken withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
                    if (success){
                        [task.comments removeAllObjects];
                        [task.comments addObjectsFromArray:((PPTask*)object).comments];
                        if (self.isTaken){
                            if (self.commentsCount == 0){
                                task.status = WORK_STARTED;
                            }
                            else{
                                PPComment* last = [task.comments objectAtIndex:0];
                                if (isArtist)
                                    task.status = (last.isArtist ? PENDING_FOR_REVIEW : PENDING);
                                else
                                    task.status = (last.isArtist ? PENDING : PENDING_FOR_REVIEW);
                            }
                        }
                        if ([self.delegate respondsToSelector:@selector(onCommentAdded)])
                            [self.delegate onCommentAdded];
                    }
                    else{
                        if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)]){
                            [self.delegate onOperationFailed:TASK_OP_COMMENT_ADD withError:error];
                        }
                    }
  
                }];
            }
        }];
    }
}

-(void) removeCommentAtIndex:(int)index{
    PPComment* c = [self commentAtIndex:index];
    if (c == nil){
        if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)]){
            [self.delegate onOperationFailed:TASK_OP_NONE withError:[[TSError alloc] initWithCode: TS_ERROR_INTERNAL Title:@"Invalid comment's index was given."]];

        }
        return;
    }
    
    [self removeCommentWithId:c.ID];
}

-(void) removeCommentWithId:(int)commentId{
    if (![self ensureIsReady]) return;
    
    [[PPRequestManager sharedManager] requestDeleteCommentWithID:commentId forUser:userToken withResopnseCallback:^(BOOL success, NSObject* object, TSError *error) {
        if (success){
            [task.comments removeAllObjects];
            [task.comments addObjectsFromArray:((PPTask*)object).comments];
            if (self.isTaken){
                if (self.commentsCount == 0){
                    task.status = PENDING;
                }
                else{
                    PPComment* last = [task.comments objectAtIndex:0];
                    if (isArtist)
                        task.status = (last.isArtist ? PENDING_FOR_REVIEW : PENDING);
                    else
                        task.status = (last.isArtist ? PENDING : PENDING_FOR_REVIEW);
                }
            }

            if ([self.delegate respondsToSelector:@selector(onCommentRemoved)])
                [self.delegate onCommentRemoved];
        }
        else{
            if ([self.delegate respondsToSelector:@selector(onOperationFailed:withError:)]){
                [self.delegate onOperationFailed:TASK_OP_COMMENT_REMOVE withError:error];
            }
        }
    }];
}


@end
