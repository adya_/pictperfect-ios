#import <Foundation/Foundation.h>

@class PPTask;
@class PPBid;
@class PPComment;
@class PPPicture;
@class PPUser;
@class TSError;

typedef enum {
    TASK_OP_NONE,
    TASK_OP_TASK_LOAD_DETAILS,
    TASK_OP_TASK_ACCEPT,
    TASK_OP_TASK_CANCEL,
    TASK_OP_BID_PLACE,
    TASK_OP_BID_CANCEL,
    TASK_OP_BID_SELECT,
    TASK_OP_COMMENT_PICTURE,
    TASK_OP_COMMENT_ADD,
    TASK_OP_COMMENT_REMOVE
} TaskOperation;

@protocol PPTaskManagerCallbacks <NSObject>

@optional -(void) onTaskDetailsReceived:(PPTask*) task;
@optional -(void) onTaskAccepted;
@optional -(void) onTaskCanceled;

@optional -(void) onCommentPictureUploaded:(PPPicture*)picture;
@optional -(void) onCommentAdded;
@optional -(void) onCommentRemoved;

@optional -(void) onBidPlaced:(PPBid*) placedBid;
@optional -(void) onBidCanceled;
@optional -(void) onBidSelected:(PPBid*)selectedBid;

@optional -(void) onOperationFailed:(TaskOperation)operation withError:(TSError*) error;

@end

@interface PPTaskManager : NSObject

-(id) initWithUser:(PPUser*) user;

@property (readonly) PPTask* task;

@property (readonly) NSArray* comments;
@property (readonly) NSArray* bids;

@property (readonly) int commentsCount;
@property (readonly) int bidsCount;

@property (readonly) PPBid* confirmedBid;
@property (readonly) PPBid* placedBid;

@property (readonly) BOOL isReady;
@property id<PPTaskManagerCallbacks> delegate;

@property (readonly) BOOL isArtist;
@property (readonly) BOOL isPendingArtist;
@property (readonly) BOOL isTaken;
@property (readonly) BOOL isProgressed;
@property (readonly) BOOL isFinished;
@property (readonly) BOOL isOwnTask;
@property (readonly) BOOL hasArtistPicture;
@property (readonly) BOOL isDeadlineReached;

-(PPComment*) commentAtIndex:(int)index;
-(PPBid*) bidAtIndex:(int)index;

-(void) getTaskDetailsForTaskWithId:(int) taskId;
-(void) acceptTaskWithArtistRating:(int) rating;
-(void) cancelTask;

-(void) selectBidWithId:(int) bidId andPayAuthId:(NSString*) authId;
-(void) placeBidWithPrice:(double) price;
-(void) cancelBid;

-(void) addCommentWithMeassage:(NSString*) message andPicture:(UIImage*) picture;

-(void) removeCommentWithId:(int) commentId;
-(void) removeCommentAtIndex:(int) index;


@end
