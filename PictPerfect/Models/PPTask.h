//
//  PPTask.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TaskStatus.h"
#import "TSJSONParselable.h"

@class PPUser;
@class PPPicture;

@interface PPTask : NSObject <TSJSONParselable>

@property (readonly) int ID;
@property (readonly) NSString* title;
@property (readonly) NSString* description;
@property (readonly) PPPicture* picture;
@property (readonly) NSString* date;
@property TaskStatus status;
@property (readonly) NSString* statusString; // text repersentation
@property (readonly) PPUser* author;

@property (readonly) int timeLeft;
@property (readonly) int timeGiven;

@property NSMutableArray* bids;
@property NSMutableArray* comments;


@end
