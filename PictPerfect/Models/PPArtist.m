//
//  PPArtist.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPArtist.h"

@implementation PPArtist
    @synthesize rating;
    @synthesize votesCount;

-(id) init{
    self = [super init];
    self.role = ARTIST;
    return self;
}

-(id) initWithFirstName:(NSString *)userFirstName andLastName:(NSString *)userLastName andRating:(double)artistRating withAmountVotes:(int)votes{
    self = [super initWithFirstName:userFirstName andLastName:userLastName];
    rating = artistRating;
    votesCount = votes;
    return self;
}
@end
