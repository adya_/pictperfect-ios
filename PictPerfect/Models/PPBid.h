//
//  PPBid.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"
@class PPArtist;

@interface PPBid : NSObject <TSJSONParselable>
@property (readonly) int ID;
@property (readonly) double price;
@property (readonly) BOOL isConfirmed;
@property (readonly) PPArtist* artist;

@end
