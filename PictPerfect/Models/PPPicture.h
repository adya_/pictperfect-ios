//
//  PPPicture.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

@interface PPPicture : NSObject <TSJSONParselable>

@property (readonly) int ID;
@property (readonly) NSString* imageURL;
@property (readonly) NSString* thumbURL;
@property (readonly) BOOL isValid;
@end
