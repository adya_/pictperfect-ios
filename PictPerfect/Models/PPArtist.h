//
//  PPArtist.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPUser.h"

@interface PPArtist : PPUser
    @property (readonly) double rating;
    @property (readonly) int votesCount;

-(id) initWithFirstName:(NSString *)userFirstName andLastName:(NSString *)userLastName andRating:(double) artistRating withAmountVotes:(int) votes;
@end
