//
//  PPUser.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPUser.h"
#import "TSUtils.h"
@implementation PPUser

@synthesize firstName;
@synthesize lastName;

@synthesize email;

@synthesize shouldChangePassword;

@synthesize token;
@synthesize role;

-(id) init{
    self = [super init];
    self.role = USER;
    token = nil;
    email = nil;
    firstName = nil;
    lastName = nil;
    return self;
}

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    token = nonNull([jsonObject valueForKey:@"access_token"]);
    role = [[jsonObject valueForKey:@"Role"] integerValue];
    shouldChangePassword = [[jsonObject valueForKey:@"ProposePSWDchange"] boolValue];
    return self;
}

-(id) initWithUsername:(NSString *)username andToken:(NSString*)userToken{
    self = [self init];
    email = username;
    token = userToken;
    return self;
}

-(id) initWithFirstName:(NSString *)userFirstName andLastName:(NSString *)userLastName{
    self = [self init];
    firstName = userFirstName;
    lastName = userLastName;
    return self;
}


@end
