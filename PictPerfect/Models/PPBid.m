//
//  PPBid.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPBid.h"
#import "PPArtist.h"
#import "TSUtils.h"

@implementation PPBid

@synthesize ID;
@synthesize price;
@synthesize isConfirmed;
@synthesize artist;

-(id) init {
    self = [super init];
    artist = [PPArtist new];
    return self;
}

-(id) initWithJSON:(NSDictionary*) jsonObject{
    self = [self init];
    ID = [[jsonObject valueForKey:@"Id"] integerValue];
    isConfirmed = [[jsonObject valueForKey:@"IsConfirmed"] boolValue];
    price = [[jsonObject valueForKey:@"Price"] doubleValue];

    NSString* firstName = nonNull([jsonObject valueForKey:@"ArtUserFirstName"]);
    NSString* lastName = nonNull([jsonObject valueForKey:@"ArtUserLastName"]);
    double rating = [[jsonObject valueForKey:@"ArtUserRating"] doubleValue];
    int votes = [[jsonObject valueForKey:@"ArtUserVotes"] intValue];

    artist = [[PPArtist alloc] initWithFirstName:firstName andLastName:lastName andRating:rating withAmountVotes:votes];

    return self;
}

-(BOOL) isEqual:(id)object{
    return [object isMemberOfClass:[PPBid class]] && (((PPBid*)object).ID == self.ID);
}
@end
