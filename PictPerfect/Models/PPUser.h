//
//  PPUser.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserRole.h"

#import "TSJSONParselable.h"

@interface PPUser : NSObject <TSJSONParselable>

@property (readonly) NSString* firstName;
@property (readonly) NSString* lastName;

@property (readonly) NSString* email;

@property BOOL shouldChangePassword;

@property (readonly) NSString* token;
@property UserRole role;

-(id) initWithUsername:(NSString*) username andToken:(NSString*) userToken;

-(id) initWithFirstName:(NSString*) userFirstName andLastName:(NSString*) userLastName;
@end
