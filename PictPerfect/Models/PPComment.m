//
//  PPComment.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPComment.h"
#import "PPUser.h"
#import "PPArtist.h"
#import "PPPicture.h"
#import "TSUtils.h"

@implementation PPComment

    @synthesize ID;
    @synthesize text;
    @synthesize picture;
    @synthesize author;
    @synthesize date;
    @synthesize isArtist;

-(id) init{
    self = [super init];
    picture = [PPPicture new];
    author = [PPUser new];
    return self;
}

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    ID = [[jsonObject valueForKey:@"Id"] intValue];
    
    NSString* fn = nonNull([jsonObject valueForKey:@"user_FirstName"]);
    NSString* ln = nonNull([jsonObject valueForKey:@"user_LastName"]);
    
    isArtist = [[jsonObject valueForKey:@"isArtist"] boolValue];
    author = [[PPUser alloc] initWithFirstName:fn andLastName:ln];
    picture = [[PPPicture alloc] initWithJSON:jsonObject];
    
    text = nonNull([jsonObject valueForKey:@"Text"]);
    date = nonNull([jsonObject valueForKey:@"CreateTime"]);
    date = [TSUtils formatToLocalFromUTCDateTime:self.date];

    return self;
}

-(BOOL) isEqual:(id)object{
    return [object isMemberOfClass:[PPComment class]] && (((PPComment*)object).ID == self.ID);
}

@end
