//
//  PPComment.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

@class PPPicture;
@class PPUser;

@interface PPComment : NSObject <TSJSONParselable>
    @property (readonly) int ID;
    @property (readonly) NSString* text;
    @property (readonly) PPPicture* picture;
    @property (readonly) PPUser* author;
    @property (readonly) NSString* date;
/// Indicates whether author is artist. (Works only when pict is taken)
    @property (readonly) BOOL isArtist;
@end
