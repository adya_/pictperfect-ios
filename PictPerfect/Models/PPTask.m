//
//  PPTask.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPTask.h"
#import "PPUser.h"
#import "PPPicture.h"
#import "PPBid.h"
#import "PPComment.h"
#import "TSUtils.h"

#define STATUS_CREATED @"Pict Not Perfect"
// this one should have also "Artist Bid Received"
#define STATUS_ARTIST_SELECTION @"Pending User"
#define STATUS_IN_WORK @"Pending"
#define STATUS_REVIEW @"Awaiting User Acceptance"
#define STATUS_WORK_STARTED @"Work Started"
#define STATUS_DONE @"Pict Perfect"
#define STATUS_UNKNOWN @"Unknown"

@implementation PPTask

@synthesize ID;
@synthesize title;
@synthesize description;
@synthesize picture;
@synthesize date;
@synthesize status;
@synthesize author;
@synthesize timeLeft;
@synthesize timeGiven;
@synthesize bids;
@synthesize comments;

-(NSString*) statusString{
    switch (status) {
        case CREATED:
            return STATUS_CREATED;
        case PENDING_ARTIST_SELECTION:
            return STATUS_ARTIST_SELECTION;
        case IN_WORK:
            return STATUS_IN_WORK;
        case PENDING_FOR_REVIEW:
            return STATUS_REVIEW;
        case WORK_STARTED:
            return STATUS_WORK_STARTED;
        case DONE:
            return STATUS_DONE;
        default:
            return STATUS_UNKNOWN;
    }
}

-(PPTask*) init{
    self = [super init];
    bids = [NSMutableArray new];
    comments = [NSMutableArray new];
    picture = [PPPicture new];
    author = [PPUser new];
    return self;
}

-(id) initWithJSON:(NSDictionary*) jsonObject{
    self = [self init];
    ID = [[jsonObject valueForKey:@"Id"] integerValue];
    self.status = [[jsonObject valueForKey:@"state"] integerValue];
    if (self.status == 0)
       self.status = [[jsonObject valueForKey:@"StateId"] integerValue];
    title = nonNull([jsonObject valueForKey:@"Task"]);
    description = nonNull([jsonObject valueForKey:@"Descr"]);
    date = nonNull([jsonObject valueForKey:@"CreateTime"]);
    if (date)
        date = [TSUtils formatToLocalFromUTCDateTime:self.date];
    id tL = nonNull([jsonObject valueForKey:@"TimeLeft"]);
    timeLeft = (tL != nil ? [tL intValue] : -1);
    id tG = nonNull([jsonObject valueForKey:@"TimeToDo"]);
    timeGiven = (tG != nil ? [tG intValue] : -1);
    picture = [[PPPicture alloc] initWithJSON:jsonObject];
    NSString* firstName = nonNull([jsonObject valueForKey:@"first_name"]);
    NSString* lastName = nonNull([jsonObject valueForKey:@"last_name"]);
    if (!firstName || !lastName)
        author = nil;
    else
        author = [[PPUser alloc] initWithFirstName:firstName andLastName:lastName];
    [self initBidsList:[jsonObject valueForKey:@"Bids"]];
    [self initCommentsList:[jsonObject valueForKey:@"Comments"]];
    return self;
}

-(void) initBidsList:(NSArray*) jsonArray{
    for (NSDictionary* dic in jsonArray) {
        [bids addObject:[[PPBid alloc] initWithJSON:dic]];
    }
}

-(void) initCommentsList:(NSArray*) jsonArray{
    for (NSDictionary* dic in jsonArray) {
        [comments addObject:[[PPComment alloc] initWithJSON:dic]];
    }
}

-(BOOL) isEqual:(id)object{
    return [object isMemberOfClass:[PPTask class]] && (((PPTask*)object).ID == self.ID);
}

@end
