//
//  TaskStatus.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#ifndef PictPerfect_TaskStatus_h
#define PictPerfect_TaskStatus_h

typedef enum {
    UNDEFINED = 0,
    CREATED,
    PENDING_ARTIST_SELECTION,
    WORK_STARTED,
    IN_WORK,
    PENDING_FOR_REVIEW,
    DONE
    } TaskStatus;

#endif
