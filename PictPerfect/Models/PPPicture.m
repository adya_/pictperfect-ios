//
//  PPPicture.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPPicture.h"
#import "TSUtils.h"


@implementation PPPicture

@synthesize ID;
@synthesize imageURL;
@synthesize thumbURL;

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    ID = [[jsonObject valueForKey:@"Id"] integerValue];
    imageURL = nonNull([jsonObject valueForKey:@"image_link"]);
    thumbURL = nonNull([jsonObject valueForKey:@"thumb_link"]);
    return self;
}

-(BOOL) isValid{
    return imageURL != nil && thumbURL != nil && imageURL.length > 0 && thumbURL.length > 0;
}

-(BOOL) isEqual:(id)object{
    return [object isMemberOfClass:[PPPicture class]] && (((PPPicture*)object).ID == self.ID);
}
@end
