//
//  PPLoadingView.m
//  PictPerfect
//
//  Created by Adya on 10/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPLoadingView.h"

@implementation PPLoadingView{
    LoadingViewState curState;
    IBOutlet UILabel *loadingLabel;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}

-(id) init{
    self = [super init];
    [self setState:LV_HIDDEN];
    return self;
}

-(void) setState:(LoadingViewState)state{
    curState = state;
    switch (curState) {
        case LV_HIDDEN:
            self.hidden = YES;
            break;
        case LV_LOADING:
            self.hidden = NO;
            activityIndicator.hidden = NO;
            loadingLabel.text = self.loadingText;
            break;
        case LV_PREPARED:
            self.hidden = NO;
            activityIndicator.hidden = YES;
            loadingLabel.text = self.preparedText;
            break;
        default:
            break;
    }
}

+(CGFloat) height{
    return 60.0f;
}

@end
