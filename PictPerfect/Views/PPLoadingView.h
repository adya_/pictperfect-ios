//
//  PPLoadingView.h
//  PictPerfect
//
//  Created by Adya on 10/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {LV_PREPARED, LV_LOADING, LV_HIDDEN} LoadingViewState;

@interface PPLoadingView : UIView

@property NSString* loadingText;
@property NSString* preparedText;

-(void) setState:(LoadingViewState) state;

+(CGFloat) height;
@end
