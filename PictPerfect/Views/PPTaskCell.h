//
//  PPTaskCell.h
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PPTask;

@interface PPTaskCell : UITableViewCell

@property PPTask* task;

@property (strong, nonatomic) IBOutlet UILabel *lDate;
@property (strong, nonatomic) IBOutlet UILabel *lAuthor;

@property (strong, nonatomic) IBOutlet UIImageView *ivPicture;
@property (strong, nonatomic) IBOutlet UILabel *lTitle;
@property (strong, nonatomic) IBOutlet UILabel *lStatus;
@property (strong, nonatomic) IBOutlet UILabel *lDeadline;

@property BOOL colorTitle;

+(CGFloat) height;
@end
