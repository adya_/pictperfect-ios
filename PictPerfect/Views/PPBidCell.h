
#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@class PPBid;
@class PPPicture;

@interface PPBidCell : MGSwipeTableCell

-(void) setBid:(PPBid*) newBid withPicture:(PPPicture*)picture;

+(CGFloat) height;
@end
