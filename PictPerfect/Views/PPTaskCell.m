//
//  PPTaskCell.m
//  PictPerfect
//
//  Created by Adya on 08/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPTaskCell.h"
#import "PPTask.h"
#import "PPPicture.h"
#import "PPUser.h"
#import "PPRequestManager.h"
#import "PPStorage.h"

@implementation PPTaskCell{
    PPTask* _task;
    BOOL _colorTitle;
}

-(BOOL) colorTitle{
    return _colorTitle;
}

-(void) setColorTitle:(BOOL)colorTitle{
    _colorTitle = colorTitle;
    [self updateTask];
}

+(CGFloat) height{
    return 110
    ;
}

-(void) setTask:(PPTask *)task{
    _task = task;
    [self updateTask];
}

-(PPTask*) task{
    return _task;
}

-(void) updateTask{
    [[PPRequestManager sharedManager] requestImageFromURL:_task.picture.thumbURL forImageView:self.ivPicture withPlaceholder:nil];
    if (_task.author)
        self.lAuthor.text = [NSString stringWithFormat:@"%@ %@" , _task.author.firstName, _task.author.lastName];
    else
        self.lAuthor.text = @"";
    
    self.lDate.text = _task.date;
    self.lTitle.text = _task.title;
    self.lStatus.text = _task.statusString;
    BOOL isTaken = (_task.status != UNDEFINED &&
    _task.status != CREATED &&
    _task.status != PENDING_ARTIST_SELECTION);
    
    self.lDeadline.hidden = !isTaken;
    if (isTaken){
        int hours = _task.timeLeft;
        if (hours < 0){
            self.lDeadline.text = @"Deadline Reached!";
            self.lDeadline.textColor = STORAGE_COLORS_DEADLINE_REACHED;
        }else if (hours <= 24){
            self.lDeadline.text = @"Deadline: 1 day!";
            self.lDeadline.textColor = STORAGE_COLORS_DEADLINE_RUNS_OUT;
        }
        else{
            int days = ceil(hours / 24.0f);
            self.lDeadline.text = [NSString stringWithFormat:@"Deadline: %d days", days];
            self.lDeadline.textColor = STORAGE_COLORS_DEADLINE_DEFAULT;
        }
    }
    
    CGRect frame = self.lDeadline.frame;
    frame.origin.y = self.lAuthor.frame.origin.y + (_task.author ? self.lAuthor.frame.size.height : 0);
    self.lDeadline.frame = frame;
    
    CGFloat dlHeight = (self.lDeadline.hidden?0:self.lDeadline.frame.size.height);
    
    frame = self.lTitle.frame;
    frame.origin.y = self.lDeadline.frame.origin.y + dlHeight;
    self.lTitle.frame = frame;
    
    frame = self.lStatus.frame;
    frame.origin.y = self.lTitle.frame.origin.y + self.lTitle.frame.size.height;
    self.lStatus.frame = frame;
    
    if (self.colorTitle){
        [self colorTitleForStatus];
    }
}

-(void) colorTitleForStatus{
    switch (_task.status) {
        case DONE:
            self.lTitle.textColor = STORAGE_COLORS_DONE_TASK;
            break;
        case PENDING_FOR_REVIEW:
        case IN_WORK:
        case WORK_STARTED:
            self.lTitle.textColor = STORAGE_COLORS_WORKING_TASK;
            break;
        case PENDING_ARTIST_SELECTION:
            self.lTitle.textColor = STORAGE_COLORS_PENDING_TASK;
            break;
        default:
            self.lTitle.textColor = STORAGE_COLORS_DEFAULT_TASK_COLOR;
            break;
    }
}

@end
