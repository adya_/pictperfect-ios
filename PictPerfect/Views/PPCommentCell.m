
#import "PPCommentCell.h"
#import "PPComment.h"
#import "PPUser.h"
#import "PPPicture.h"
#import "PPRequestManager.h"
#import "PPStorage.h"

#define COMMENT_TEXT_WIDTH_WITH_PICTURE 195
#define COMMENT_TEXT_WIDTH_FULL 305
#define COMMENT_TEXT_POS_Y 55

@implementation PPCommentCell{
    IBOutlet UILabel *lAuthorName;
    IBOutlet UILabel *lDate;
    IBOutlet UILabel *lCommentText;
    
    PPComment* comment;
    UIColor* commentColor;
    
    BOOL showOwnership;
    BOOL isArtist;
}

@synthesize ivPicture;

-(void) showOwnership:(BOOL)show withRole:(BOOL)isArtistRole{
    showOwnership = show;
    isArtist = isArtistRole;
}

-(void)setComment:(PPComment *)newComment{
    [self setComment:newComment withColor:STORAGE_COLORS_COMMENT_DEFAULT];
}

-(void) setComment:(PPComment *)newComment withColor:(UIColor *)color{
    comment = newComment;
    commentColor = color;
    [self updateCell];
}

-(void) updateCell{
    lAuthorName.text = ((showOwnership && (comment.isArtist != isArtist))?@"Me":[NSString stringWithFormat:@"%@ %@", comment.author.firstName, comment.author.lastName]);
    
    lDate.text = comment.date;
    lCommentText.text = comment.text;
    
    [self setPicture]; // may increase width of the lCommentText, so call it before resizeToFit.
    
    [lCommentText sizeToFit];
}

-(void) setPicture{
    if (comment.picture.isValid){
        [[PPRequestManager sharedManager] requestImageFromURL:comment.picture.thumbURL forImageView:ivPicture withPlaceholder:nil];
    }
    else{

        CGRect frame = CGRectMake(ivPicture.frame.origin.x + 30.0f, lCommentText.frame.origin.y, ivPicture.frame.size.width + lCommentText.frame.size.width + 30.0f, lCommentText.frame.size.height);
        lCommentText.frame = frame;
        
        frame = CGRectMake(ivPicture.frame.origin.x + 40.0f, lAuthorName.frame.origin.y, ivPicture.frame.size.width + lAuthorName.frame.size.width + 40.0f, lAuthorName.frame.size.height);
        lAuthorName.frame = frame;
    }
}

+(CGFloat) calculateHeightWithComment:(PPComment *)comment{
    CGFloat height = (comment.picture ? 110.0f : 65.0f);
    CGFloat width = (comment.picture ? COMMENT_TEXT_WIDTH_WITH_PICTURE : COMMENT_TEXT_WIDTH_FULL);
    NSString *text = comment.text;
    UIFont *font = [UIFont systemFontOfSize:14];// The font should be the same as that of your textView
    CGSize constraintSize = CGSizeMake(width, CGFLOAT_MAX);
    
    CGSize size = [text sizeWithFont:font constrainedToSize:constraintSize lineBreakMode: UILineBreakModeWordWrap];
    
    if (size.height + COMMENT_TEXT_POS_Y > height){
        height = size.height + COMMENT_TEXT_POS_Y;
    }
    
    return height;
}

@end
