#import "PPBidCell.h"
#import "PPBid.h"
#import "PPPicture.h"
#import "PPArtist.h"
#import "PPRequestManager.h"
#import "DYRateView.h"

@implementation PPBidCell{
    IBOutlet UILabel *lArtistName;
    IBOutlet UIView *vRatingPlaceholder;
    IBOutlet UILabel *lRatingVotes;
    IBOutlet UILabel *lPrice;
    IBOutlet UIImageView *ivPicture;
    PPBid* bid;
    PPPicture* picture;

}

-(void) setBid:(PPBid*) newBid withPicture:(PPPicture*)taskPicture{
    bid = newBid;
    picture = taskPicture;
    [self updateCell];
}

-(void) setSelected:(BOOL)selected{
    [super setSelected:NO];
}

-(void) setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:NO animated:animated];
}

-(void) updateCell{
    [[PPRequestManager sharedManager] requestImageFromURL:picture.thumbURL forImageView:ivPicture withPlaceholder:nil];
    lArtistName.text = [NSString stringWithFormat:@"%@ %@", bid.artist.firstName, bid.artist.lastName ];
    lPrice.text = [NSString stringWithFormat:@"$%.2f", bid.price];
    lRatingVotes.text = [NSString stringWithFormat:@"(%d votes)", bid.artist.votesCount];
    double rating = bid.artist.rating;
    DYRateView* rateView = [[DYRateView alloc] initWithFrame:vRatingPlaceholder.frame];
    [self.contentView addSubview:rateView];
    rateView.rate = rating;
    
}


+(CGFloat) height{
    return 110.0f;
}



@end
