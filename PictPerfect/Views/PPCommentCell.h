
#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
@class PPComment;

@interface PPCommentCell : MGSwipeTableCell

@property (strong, nonatomic) IBOutlet UIImageView *ivPicture;
-(void) setComment:(PPComment*) newComment withColor:(UIColor*) color;
-(void) setComment:(PPComment *)newComment;

-(void) showOwnership:(BOOL)show withRole:(BOOL) isArtistRole;

+(CGFloat) calculateHeightWithComment:(PPComment*)comment;
@end
