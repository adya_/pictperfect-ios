//
//  PPAppDelegate.m
//  PictPerfect
//
//  Created by Adya on 07/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "PPAppDelegate.h"
#import "CRGradientNavigationBar.h"
#import "PPStorage.h"

@implementation PPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[CRGradientNavigationBar appearance] setBarTintGradientColors:@[ STORAGE_COLORS_SECONDARY_COLOR, STORAGE_COLORS_MAIN_COLOR]];
    NSDictionary *barItemAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                STORAGE_COLORS_TITLE_COLOR,
                                UITextAttributeTextColor,
                                [UIColor clearColor],
                                UITextAttributeTextShadowColor,
                                nil];
    
    NSDictionary *navBarAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor whiteColor],
                                UITextAttributeTextColor,
                                [UIColor clearColor],
                                UITextAttributeTextShadowColor,
                                [UIColor whiteColor],
                                NSForegroundColorAttributeName,
                                nil];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes: navBarAttributes
                                                forState: UIControlStateNormal];
    
    [[CRGradientNavigationBar appearance] setTitleTextAttributes:navBarAttributes];
        //set back button arrow color
    [[CRGradientNavigationBar appearance] setTintColor:[UIColor whiteColor]];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
