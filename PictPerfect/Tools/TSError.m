
#import "TSError.h"

@implementation TSError

@synthesize title;
@synthesize description;
@synthesize code;


-(id) init{
    self = [super init];
    self.title = @"";
    self.description = @"";
    return  self;
}

- (TSError*) initWithCode:(NSInteger)errorCode{
    return [self initWithCode:errorCode Title:@"Error"];
}

- (TSError*) initWithCode:(NSInteger)errorCode Title:(NSString*)errorTitle{
    return [self initWithCode:errorCode Title:errorTitle andDescription:@""];
}

- (TSError*) initWithCode:(NSInteger)errorCode Title:(NSString *)errorTitle andDescription:(NSString*) errorDescription{
    TSError* err = [[TSError alloc] init];
    err.code = errorCode;
    err.title = errorTitle;
    err.description = errorDescription;
    return err;
}

+ (TSError*) errorWithCode:(NSInteger)code{
    return [self errorWithCode:code Title:@""];
}

+ (TSError*) errorWithCode:(NSInteger)code Title:(NSString*)title{
    return [self errorWithCode:code Title:title andDescription:@""];
}

+(TSError*) errorWithCode:(NSInteger)code Title:(NSString *)title andDescription:(NSString *)description{
    return [[TSError alloc] initWithCode:code Title:title andDescription:description];
}
@end
