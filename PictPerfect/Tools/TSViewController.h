//
//  TSViewController.h
//  PictPerfect
//
//  Created by Adya on 13/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSViewController : UIViewController <UITextFieldDelegate>

@property BOOL showBackButton;


-(void) dismissKeyboard;

@end
