#import <Foundation/Foundation.h>

@class TSError;

// Logs and notifies (popup) messages
@interface TSNotifier : NSObject

+(void) log:(NSString*) message;
+(void) logWithTitle:(NSString*) title message:(NSString*) msg;
+(void) logMethod:(NSString*) method;

+(void) notify:(NSString*) message;
+(void) notifyWithTitle:(NSString*) title message: (NSString*) msg;

+(void) notifyWithTitle:(NSString *)title message:(NSString *)msg acceptButton:(NSString*) ok;

+(void) notify:(NSString *)message acceptButton:(NSString*)ok;

+(void) notifyWithTitle:(NSString *)title message:(NSString *)msg acceptButton:(NSString*) ok cancelButton:(NSString*) cancel;

+(void) notifyError:(TSError*)error;
+(void) notifyError:(TSError*)error acceptButton:(NSString*)ok;
+(void) notifyError:(TSError*)error acceptButton:(NSString*)ok cancelButton:(NSString*)cancel;

+(void) showProgressOnView:(UIView*)view;
+(void) showProgressOnView:(UIView *)view withMessage:(NSString*)message;

+(void) hideProgressOnView:(UIView*)view;
@end
