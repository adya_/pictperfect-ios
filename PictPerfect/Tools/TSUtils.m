//
//  PPUtils.m
//  PictPerfect
//
//  Created by Adya on 13/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import "TSUtils.h"
#define DATE_PATTERN @"(.*)-(.*)-(.*)T(.*):(.*):(.*)\\.(.*)"
#define REGEX_URL_PATTERN @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
#define BUNDLE_ID_APP_NAME @"CFBundleDisplayName"

@implementation TSUtils
    
+(NSString*) formatDateTime:(NSString*)dateTime{
    if (!dateTime) return nil;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:DATE_PATTERN options:0 error:nil];
    
    return [regex stringByReplacingMatchesInString:dateTime options:0 range:NSMakeRange(0, dateTime.length) withTemplate:[NSString stringWithFormat:@"%@-%@-%@ %@:%@",@"$2",@"$3",@"$1",@"$4",@"$5"]];
}

+(NSString*) formatToLocalFromUTCDateTime:(NSString*)dateTime{
    NSString* dTime = [self formatDateTime:dateTime];
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [df_utc setDateFormat:@"MM-dd-yyyy HH:mm"];
    
    NSDate* date = [df_utc dateFromString:dTime];
    
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone defaultTimeZone]];
    [df_local setDateFormat:@"MM-dd-yyyy HH:mm"];
    return [df_local stringFromDate:date];

}


+ (NSString *)trim:(NSString *)target{
    if (![TSUtils nonNull:target]) return nil;
    return [target stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+(NSString*) appName{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:BUNDLE_ID_APP_NAME];
}

+(id) nonNull:(id)target{
    return ([[NSNull null] isEqual:target] ? nil : target);
}

+(UIColor*) colorWithRed:(int)red green:(int)green blue:(int)blue{
    return [TSUtils colorWithRed:red green:green blue:blue alpha:255];
}

+(UIColor*) colorWithRed:(int)red green:(int)green blue:(int)blue alpha:(int)alpha{
    return [UIColor colorWithRed:((float)red)/255.0f green:((float)green)/255.0f blue:((float)blue)/255.0f alpha:((float)alpha)/255.0f];
}

+ (BOOL) isValidURL: (NSString *) url {
    NSPredicate* urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", REGEX_URL_PATTERN];
    return [urlTest evaluateWithObject:url];
}


@end
