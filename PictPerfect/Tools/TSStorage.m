#import "TSStorage.h"

@implementation TSStorage


-(id) init{
    self = [super init];
    if (self != nil){
        values = [NSMutableDictionary new];
    }
    return self;
}

-(void) putValue:(id)value forKey:(NSString *)key{
    [values setObject:value forKey:key];
}

-(id) getValueForKey:(NSString *)key{
    return [values valueForKey:key];
}

-(void) removeValueForKey:(NSString *)key{
    [values removeObjectForKey:key];
}
@end
