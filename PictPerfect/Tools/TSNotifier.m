#import "TSNotifier.h"
#import "MBProgressHUD.h"
#import "TSError.h"

#define APP_NAME [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]

@implementation TSNotifier

+(void) log:(NSString*) message{
    [self logWithTitle:@"TSNotifier" message:message];
}

+(void) logWithTitle:(NSString*) title message:(NSString*) msg{
    NSLog(@"%@ : %@", title, msg);
}

+(void) logMethod:(NSString *)method{
    [self logWithTitle:@"NOT IMPLEMENTED" message:method];
}


+(void) notify:(NSString*) message{
    [self notifyWithTitle:APP_NAME message:message];
}

+(void) notify:(NSString *)message acceptButton:(NSString *)ok{
    [self notifyWithTitle:APP_NAME message:message acceptButton:ok];
}

+(void) notifyWithTitle:(NSString*) title message: (NSString*) msg{
    [self notifyWithTitle:title message:msg acceptButton:@"OK"];
}
+(void) notifyWithTitle:(NSString *)title message:(NSString *)msg acceptButton:(NSString*) ok{
    [self notifyWithTitle:title message:msg acceptButton:ok cancelButton:nil];
}


+(void) notifyWithTitle:(NSString *)title message:(NSString *)msg acceptButton:(NSString*) ok cancelButton:(NSString*) cancel{
    [self logWithTitle:title message:msg];
    
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:ok otherButtonTitles:cancel, nil];
    [alert show];
}

+(void) notifyError:(TSError *)error{
    [self notifyError:error acceptButton:@"OK"];
}
+(void) notifyError:(TSError*)error acceptButton:(NSString*)ok{
    [self notifyError:error acceptButton:ok cancelButton:nil];
}
+(void) notifyError:(TSError*)error acceptButton:(NSString*)ok cancelButton:(NSString*)cancel{
    [self notifyWithTitle:error.title message:error.description acceptButton:ok cancelButton:cancel];
}

+(void) showProgressOnView:(UIView *)view{
    [self showProgressOnView:view withMessage:@"Hold on..."];
}
+(void) showProgressOnView:(UIView*)view withMessage:(NSString*)message{
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    
    [hud setLabelText:message];
    [hud setMode:MBProgressHUDModeIndeterminate];
}

+(void) hideProgressOnView:(UIView*)view{
    [MBProgressHUD hideHUDForView:view animated:NO];
}

@end