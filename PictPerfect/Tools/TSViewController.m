
#import "TSViewController.h"
#import "TSUtils.h"

@implementation TSViewController{
    UITextField* currentResponder;
}

-(void) setShowBackButton:(BOOL)showBackButton{
    self.navigationItem.hidesBackButton = !showBackButton;
}

-(BOOL) showBackButton{
    return !self.navigationItem || !self.navigationItem.hidesBackButton;
}

///TODO: Implement mechanism to wrap root view with scroll view to be able to scroll content when editing some fields.
// Tips: make this wrapping only if there are text fields
// Tips: or if the wraping flag is set (add this flag)

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    // dismiss keyboard when touch outside it.
//	UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
//    [self.view addGestureRecognizer:tap];
    
    [self registerAllTextFields];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0.0")) // set white color to nav. bar in ios6 and greater. (This will fix cases when nav bar has weird darker color.)
        self.navigationController.view.backgroundColor = [UIColor whiteColor];
}

-(void) registerAllTextFields{
    for (UITextField* field in self.view.subviews) {
        if ([field isMemberOfClass:[UITextField class]])
            field.delegate = self;
    }
}

-(void) dismissKeyboard{
    [self.view endEditing:YES];
}

-(void) textFieldDidBeginEditing:(UITextField *)textField{
    currentResponder = textField;
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    currentResponder = nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
@end
