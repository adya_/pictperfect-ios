//
//  UILabel+AutoSize.h
//  PictPerfect
//
//  Created by Adya on 11/02/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (AutoSize)

-(float)resizeToFit;
-(float)expectedHeight;

@end
