//
//  PPUtils.h
//  PictPerfect
//
//  Created by Adya on 13/01/2015.
//  Copyright (c) 2015 vvteam. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifndef UTILS
#define UTILS

/*
 *  Replaces NSNull object with plain nil value.
 */

#define nonNull(value) [TSUtils nonNull:value]


/*
 *  NSString shortcuts
 */
#define trim(string) [TSUtils trim:string]


#define isValidURL(url) [TSUtils isValidURL:url]

/*
 * UIColor shortcuts with int values
 */
#define colorARGB(a,r,g,b) [TSUtils colorWithRed:r green:g blue:b alpha:a]

#define colorRGB(r,g,b) [TSUtils colorWithRed:r green:g blue:b]

/*
 *  Shortuct to application name
 */
#define APP_NAME [TSUtils appName]

/*
 *  System Versioning Preprocessor Macros
 */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#endif

@interface TSUtils : NSObject
+(NSString*) formatDateTime:(NSString*)dateTime;
+(NSString*) formatToLocalFromUTCDateTime:(NSString*)dateTime;

+(NSString*) trim:(NSString*) target;
+(BOOL) isValidURL:(NSString*) url;

+(NSString*) appName;
+(id) nonNull:(id)target;
+(UIColor*) colorWithRed:(int) red green:(int) green blue:(int)blue;
+(UIColor*) colorWithRed:(int) red green:(int) green blue:(int)blue alpha:(int)alpha;
@end
