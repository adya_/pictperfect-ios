#ifndef TS_STORAGE_H
#define TS_STORAGE_H

#import <Foundation/Foundation.h>

// Best practice to use this storage:
// NOTE: Do not use this class directly!

// 1. Subclass TSStorage.
// 2. Define your keys for this storage.
// 3. Feel fre to use it.

@interface TSStorage : NSObject{
    NSMutableDictionary* values;
}

-(void) putValue:(id)value forKey:(NSString*) key;
-(id) getValueForKey:(NSString*) key;
-(void) removeValueForKey:(NSString*)key;
@end
#endif
